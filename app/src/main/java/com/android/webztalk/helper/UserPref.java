package com.android.webztalk.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ANUPAM TYAGI on 6/29/2016.
 */
public class UserPref {
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    public UserPref(Context context) {
        sharedpreferences = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public void setAnotherUserName(String name) {
        editor.putString(Constants.ANOTHER_USER_NAME, name).commit();
    }

    public String getAnotherUserName() {
        return sharedpreferences.getString(Constants.ANOTHER_USER_NAME, "");
    }
    public void setUserName(String name) {
        editor.putString(Constants.USER_NAME, name).commit();
    }

    public String getUserName() {
        return sharedpreferences.getString(Constants.USER_NAME, "");
    }
    public void setUserId(String id) {
        editor.putString(Constants.USER_ID, id).commit();
    }

    public String getUserId() {
        return sharedpreferences.getString(Constants.USER_ID, "");
    }

    public void setUserPassword(String password) {
        editor.putString(Constants.USER_PASSWORD, password).commit();
    }

    public void setAutoLogin(boolean value) {
        editor.putBoolean(Constants.USER_AUTOLOGIN, value).commit();
    }

    public boolean getAutoLogin() {
        return sharedpreferences.getBoolean(Constants.USER_AUTOLOGIN, false);
    }

    public String getUserPassword() {
        return sharedpreferences.getString(Constants.USER_PASSWORD, "");
    }

    public void setUserEmail(String email) {
        editor.putString(Constants.USER_EMAIL, email).commit();
    }

    public void setUserPhone(String phone) {
        editor.putString(Constants.USER_PHONE, phone).commit();
    }

    public String getUserPhone() {
        return sharedpreferences.getString(Constants.USER_PHONE, "");
    }

    public void setUserAddress(String address) {
        editor.putString(Constants.USER_ADDRESS, address).commit();
    }

    public String getUserAddress() {
        return sharedpreferences.getString(Constants.USER_ADDRESS, "");
    }

    public void setUserBio(String bio) {
        editor.putString(Constants.USER_BIO, bio).commit();
    }

    public String getUserBio() {
        return sharedpreferences.getString(Constants.USER_BIO, "");
    }

    public void clearPref() {
        editor.clear().commit();
    }

    public String getUserEmail() {
        return sharedpreferences.getString(Constants.USER_EMAIL, "");
    }

    public void setUserImage(String image) {
        editor.putString(Constants.USER_IMAGE, image).commit();
    }

    public String getUserImage() {
        return sharedpreferences.getString(Constants.USER_IMAGE, "");
    }
    public void setLoginType(String facebook) {
        editor.putString(Constants.LOGIN_TYPE, facebook).commit();
    }

    public String getLoginType() {
        return sharedpreferences.getString(Constants.LOGIN_TYPE, "");
    }
    public void setFollowerCount(String foloower_count)
    {
        editor.putString(Constants.USER_FOLLOWER_COUNT,foloower_count).commit();
    }
    public String getFollowerCount()
    {
        return sharedpreferences.getString(Constants.USER_FOLLOWER_COUNT, "");
    }
    public void setFollowingCount(String foloower_count)
    {
        editor.putString(Constants.USER_FOLLOWING_COUNT,foloower_count).commit();
    }
    public String getFollowingCount()
    {
        return sharedpreferences.getString(Constants.USER_FOLLOWING_COUNT, "");
    }
}
