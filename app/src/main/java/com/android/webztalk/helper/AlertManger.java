package com.android.webztalk.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;


/**
 * Created by Anupam tyagi on 7/11/2016.
 */
public class AlertManger {

    private static AlertManger alert_instance=null;


    public static AlertManger getAlert_instance()
    {
        if(alert_instance==null)
            alert_instance=new AlertManger();
        return  alert_instance;
    }

    public void showAlert(String message,Context context)
    {
        try {
            new AlertDialog.Builder(context)
                    .setTitle(Constants.APP_NAME)
                    .setMessage(message)
                    .setPositiveButton("Ok", null)
                    .show();
        }
        catch (Exception e)
        {
            e.getMessage();
        }
    }

}
