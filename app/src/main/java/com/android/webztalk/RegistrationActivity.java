package com.android.webztalk;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.AlertManger;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.NetworkCheck;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegistrationActivity extends AppCompatActivity {

    private EditText name, email, password, user_name;
    private ImageButton signup;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword, input_layout_username;
    private UserPref user_pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        user_pref = new UserPref(this);
        initializeUI();
    }

    private void initializeUI() {
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_password_one);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password_two);
        input_layout_username = (TextInputLayout) findViewById(R.id.input_layout_username);
        user_name = (EditText) findViewById(R.id.user_name);
        signup = (ImageButton) findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkCheck.getInstance(RegistrationActivity.this).isNetworkAvailable()) {
                    if (validateLogin()) {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("name", name.getText().toString());
                        params.put("email", email.getText().toString());
                        params.put("password", password.getText().toString());
                        params.put("username", user_name.getText().toString());
                        CallService.getInstance().passInfromation(listener, Constants.SIGNUP_URL, params, true, "1", RegistrationActivity.this);
                    }
                } else {
                    Toast.makeText(RegistrationActivity.this, Constants.NETWORK_STRING, Toast.LENGTH_SHORT).show();
                }
            }
        });
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputLayoutName.setError(null);
                    inputLayoutPassword.setError(null);
                    inputLayoutEmail.setError(null);
                }
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputLayoutName.setError(null);
                    inputLayoutPassword.setError(null);
                    inputLayoutEmail.setError(null);
                }
            }
        });
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputLayoutName.setError(null);
                    inputLayoutPassword.setError(null);
                    inputLayoutEmail.setError(null);
                }
            }
        });
        user_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputLayoutName.setError(null);
                    inputLayoutPassword.setError(null);
                    inputLayoutEmail.setError(null);
                }
            }
        });
    }

    ApiResponseListener listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            try {
                JSONObject json_object = new JSONObject(response);
                if (json_object.getBoolean("status")) {
                    JSONObject user_object = json_object.getJSONObject("user_detail");
                    user_pref.setUserEmail(user_object.getString("useremail"));
                    user_pref.setUserName(user_object.getString("firstname"));
                    user_pref.setAnotherUserName(user_object.getString("username"));
                    user_pref.setLoginType("auto");
                    Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                } else {
                    AlertManger.getAlert_instance().showAlert("Either Email id or Password is invalid.", RegistrationActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private boolean validateLogin() {
        if (name.getText().length() > 0) {
            if (user_name.getText().length() > 0) {
                if (Validation.isValidEmail(email.getText().toString()) && email.getText().toString().length() > 0) {
                    if (password.getText().toString().length() > 0) {
                        return true;
                    } else {
                        inputLayoutPassword.setError("Please Enter Password");
                        password.requestFocus();
                        return false;
                    }
                } else {
                    inputLayoutEmail.setError("Please Enter Valid Email Id");
                    email.requestFocus();
                    return false;

                }
            } else {
                input_layout_username.setError("Please Enter User Name");
                user_name.requestFocus();
                return false;
            }
        } else {
            inputLayoutName.setError("Please Enter Name");
            name.requestFocus();
            return false;
        }
    }
}
