package com.android.webztalk.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.onLoadMoreListener;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 8/25/2016.
 */
public class MovieAdapter extends RecyclerView.Adapter{

    private ArrayList<MediaObject>  movie_list;
    private Context context;
    private AdapterClickListener adapter_listener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private onLoadMoreListener onListener;
    private String string_type;
    int pos;

  public MovieAdapter(ArrayList<MediaObject> movie_list, Context context, AdapterClickListener adapter_listener, RecyclerView recycler_view) {
        this.movie_list=movie_list;
        this.context=context;
        this.adapter_listener=adapter_listener;
        if (recycler_view.getLayoutManager() instanceof LinearLayoutManager) {

            Log.e("yes", "yes");
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recycler_view
                    .getLayoutManager();


            recycler_view
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            Log.e("on scroll", "on scroll");
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                            if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onListener != null) {
                                    onListener.onLoadMore();
                                }
                                loading = true;
                            }

                        }
                    });
        } else {
            Log.e("no", "no");
        }
    }
    public MovieAdapter(ArrayList<MediaObject> movie_list, Context context, AdapterClickListener adapter_listener, RecyclerView recycler_view,String type)
    {
        this.movie_list=movie_list;
        this.context=context;
        this.adapter_listener=adapter_listener;
        this.string_type=type;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, final  int position) {
        if (holder instanceof MyViewHolder) {
            ((MyViewHolder) holder).bind(position, adapter_listener);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return movie_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }
    @Override
    public int getItemCount() {
        return movie_list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v=null;
            if(string_type!=null) {
                v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.grid_layout, parent, false);
            }
            else
            {
                v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.list_row, parent, false);
            }

            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }
    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }
    public void setOnLoadMoreListener(onLoadMoreListener onListener) {
        this.onListener = onListener;
    }

    public void setLoaded(boolean value) {
        loading = value;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView user_img,img_android;
        public Button follow;
        public Spinner spinner_list;
        public TextView name,type,movie_name;
        public MyViewHolder(View view) {
            super(view);
            if(string_type!=null) {
                img_android = (ImageView) view.findViewById(R.id.img_android);
                movie_name=(TextView) view.findViewById(R.id.movie_name);
                spinner_list=(Spinner) view.findViewById(R.id.spinner_list);
            }
            else {
                user_img = (ImageView) view.findViewById(R.id.user_img);
                name = (TextView) view.findViewById(R.id.name);
                type = (TextView) view.findViewById(R.id.type);
                follow = (Button) view.findViewById(R.id.follow);
            }
        }


        public void bind(final int position, final AdapterClickListener adapter_listener) {
            if(string_type!=null)
            {
               movie_name.setText(movie_list.get(position).getMediatitle());
                Picasso.with(context)
                        .load(movie_list.get(position).getImage())
                        .placeholder(R.drawable.girl_image) // optional
                        .error(R.drawable.girl_image)// optional
                        .into(img_android);
                spinner_list.setVisibility(View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("on click", "on click");
                        adapter_listener.clickItem(position,"horizontal");
                    }
                });
            }
            else {
                type.setText(movie_list.get(position).getCategoryname());
                if (movie_list.get(position).getMediatitle().length() > 10) {
                    String new_name = movie_list.get(position).getMediatitle().substring(0, 10);
                    Log.e("name", new_name);
                    name.setText(new_name + "...");
                } else
                    name.setText(movie_list.get(position).getMediatitle());
                follow.setText("Add");
                Picasso.with(context)
                        .load(movie_list.get(position).getImage())
                        .placeholder(R.drawable.man) // optional
                        .error(R.drawable.man)// optional
                        .into(user_img);
                follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pos = position;
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("user_id", new UserPref(context).getUserId());
                        params.put("media_id", movie_list.get(pos).getId());
                        params.put("category_id", movie_list.get(pos).getCategoryid());
                        CallService.getInstance().passInfromation(api_listner, Constants.ADD__MYWEB_URL, params, true, "1", context);
                    }
                });
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("on click", "on click");
                        adapter_listener.clickItem(position,"movie");
                    }
                });
            }

        }
    }
    ApiResponseListener api_listner=new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response",response);
            if(response!=null)
            {
                try {
                    JSONObject json_object=new JSONObject(response);
                    if(json_object.getBoolean("status"))
                    {
                        UtilityPermission.showToast(context,json_object.getString("message"));
                    }
                    else
                    {
                        UtilityPermission.showToast(context,json_object.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
