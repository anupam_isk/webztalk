package com.android.webztalk.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.webztalk.AfterSplashActivity;
import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.HomeActivity;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.ImageTrans_CircleTransform;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam Tyagi on 8/25/2016.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {
    private ArrayList<User> user_list;
    private Context context;
    private AdapterClickListener adapter_listener;
    private String media_id,category_id;
    int pos;
    public UserAdapter(ArrayList<User> user_list, Context context,AdapterClickListener adapter_listener) {
        this.user_list = user_list;
        this.context = context;
        this.adapter_listener=adapter_listener;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(user_list.get(position).getFirstname());
        holder.type.setVisibility(View.GONE);
        holder.follow.setText(user_list.get(position).getFollow_status());
        Picasso.with(context)
                .load(user_list.get(position).getUserimage())
                .error(R.drawable.man)
                .resize(150, 150)
                .transform(new ImageTrans_CircleTransform(context))
                .into(holder.user_img);
        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                if(user_list.get(position).getFollow_status().equalsIgnoreCase("UNFOLLOW") ||  user_list.get(position).getFollow_status().equalsIgnoreCase("FOLLOWING"))
                 showPopup();
                else
                 sendToServer();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click", "on click");
                goItem(position);
            }
        });
    }
    private void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Constants.APP_NAME);
        builder.setMessage("do you want to unfollow this person ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendToServer();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
    private void sendToServer() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", new UserPref(context).getUserId());
        params.put("follower_id", user_list.get(pos).getId());
        CallService.getInstance().passInfromation(api_listner, Constants.FOLLOW_USER_URL, params, true, "1", context);
    }
    private void goItem(int position) {
        adapter_listener.clickItem(position,"user");
    }
    @Override
    public int getItemCount() {
        return user_list.size();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView user_img;
        public Button follow;
        public TextView name, type;
        public MyViewHolder(View view) {
            super(view);
            user_img = (ImageView) view.findViewById(R.id.user_img);
            name = (TextView) view.findViewById(R.id.name);
            type = (TextView) view.findViewById(R.id.type);
            follow = (Button) view.findViewById(R.id.follow);
        }
    }
    ApiResponseListener api_listner = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if(request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            user_list.get(pos).setFollow_status(json_object.getString("follow_status"));
                            notifyDataSetChanged();
                        } else {
                            UtilityPermission.showToast(context, "Some thing went wrong");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
}
