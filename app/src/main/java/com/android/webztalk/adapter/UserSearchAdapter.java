package com.android.webztalk.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.onLoadMoreListener;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam Tyagi on 8/25/2016.
 */
public class UserSearchAdapter extends RecyclerView.Adapter {
    private ArrayList<User> user_list;
    private Context context;
    private AdapterClickListener adapter_listener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private onLoadMoreListener onListener;
    String media_id, category_id;
    int pos;

    public UserSearchAdapter(ArrayList<User> user_list, Context context, AdapterClickListener adapter_listener, String media_id, String category_id, RecyclerView recycler_view) {
        this.user_list = user_list;
        this.context = context;
        this.adapter_listener = adapter_listener;
        this.media_id = media_id;
        this.category_id = category_id;
        if (recycler_view.getLayoutManager() instanceof LinearLayoutManager) {
            Log.e("yes", "yes");
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recycler_view
                    .getLayoutManager();
            recycler_view
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            Log.e("on scroll", "on scroll");
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                            if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onListener != null) {
                                    onListener.onLoadMore();
                                }
                                loading = true;
                            }

                        }
                    });
        } else {
            Log.e("no", "no");
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            ((MyViewHolder) holder).bind(position, adapter_listener);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return user_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setOnLoadMoreListener(onLoadMoreListener onListener) {
        this.onListener = onListener;
    }

    public void setLoaded(boolean value) {
        loading = value;
    }

    @Override
    public int getItemCount() {
        return user_list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_row, parent, false);

            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView user_img;
        public Button follow;
        public TextView name, type;

        public MyViewHolder(View view) {
            super(view);
            user_img = (ImageView) view.findViewById(R.id.user_img);
            name = (TextView) view.findViewById(R.id.name);
            type = (TextView) view.findViewById(R.id.type);
            follow = (Button) view.findViewById(R.id.follow);
        }

        public void bind(final int position, final AdapterClickListener adapter_listener) {
            Log.e("position", position + "");
            name.setText(user_list.get(position).getFirstname());
            type.setVisibility(View.GONE);
            follow.setText("Add");
            Picasso.with(context)
                    .load(user_list.get(position).getUserimage())
                    .placeholder(R.drawable.girl_image) // optional
                    .error(R.drawable.girl_image)// optional
                    .into(user_img);
            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("user_id", new UserPref(context).getUserId());
                    params.put("otheruser_id", user_list.get(pos).getId());
                    params.put("media_id", media_id);
                    params.put("category_id", category_id);
                    CallService.getInstance().passInfromation(api_listner, Constants.ADD_RECOMMENDED_WEB_URL, params, true, "2", context);

                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapter_listener.clickItem(position, "user");
                }
            });
        }
    }

    ApiResponseListener api_listner = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            UtilityPermission.showToast(context, json_object.getString("message"));
                        } else {
                            UtilityPermission.showToast(context, json_object.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
}
