package com.android.webztalk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.Model.Filter;
import com.android.webztalk.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 9/13/2016.
 */
public class GenureAdapter extends RecyclerView.Adapter<GenureAdapter.MyViewHolder> {
     private ArrayList<Filter> map_list;
     private AdapterClickListener click_listener;
      Context context;
    public GenureAdapter(ArrayList<Filter> map_list, Context context,AdapterClickListener click_listener) {
        this.map_list=map_list;
        Log.e("chat adapter","chat adapter");
        this.context=context;
        this.click_listener=click_listener;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        holder.name.setText(map_list.get(position).getFiltername());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click_listener.clickItem(position, "genure");
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            name.setBackgroundColor(context.getResources().getColor(R.color.green_color));
        }
    }

}
