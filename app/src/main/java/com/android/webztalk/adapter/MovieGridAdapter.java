package com.android.webztalk.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 8/29/2016.
 */
public class MovieGridAdapter extends RecyclerView.Adapter<MovieGridAdapter.MyViewHolder> {
    private ArrayList<MediaObject> movie_list;
    private Context context;
    final List<String> rating_list;
    private AdapterClickListener adapter_listener;
    private SparseBooleanArray selectedItems;
    String user_id, type;
    private int some_position_for_device = 9;
    private int some_another_position_for_device = 9;
    int pos;
    private int first_position = 0, second_position = 1, third_position = 2, fourth_position = 3, fifth_position = 4, sixth_position = 5;
    private boolean check;
    String[] rating = new String[]{
            "No Rating",
            "Eh",
            "Only Ok",
            "Good ",
            "Awesome!!",
            "Epic!!"
    };

    public MovieGridAdapter(ArrayList<MediaObject> movie_list, Context context, AdapterClickListener adapter_listener, String user_id, String type) {
        this.context = context;
        this.adapter_listener = adapter_listener;
        this.user_id = user_id;
        check = false;
        this.movie_list = movie_list;
        this.type = type;
        selectedItems = new SparseBooleanArray();
        rating_list = new ArrayList<>(Arrays.asList(rating));

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_android;
        public TextView movie_name, added_text, like_text, dislike_text;
        private Spinner spinner_list;
        private ImageButton like_button, dislike_button;


        public MyViewHolder(View view) {
            super(view);
            img_android = (ImageView) view.findViewById(R.id.img_android);
            movie_name = (TextView) view.findViewById(R.id.movie_name);
            if (!type.equals("recommended")) {
                spinner_list = (Spinner) view.findViewById(R.id.spinner_list);
            } else {
                added_text = (TextView) view.findViewById(R.id.added_text);
                like_text = (TextView) view.findViewById(R.id.like_text);
                dislike_text = (TextView) view.findViewById(R.id.dislike_text);
                like_button = (ImageButton) view.findViewById(R.id.like_button);
                dislike_button = (ImageButton) view.findViewById(R.id.dislike_button);
            }
        }


    }

    @Override
    public int getItemCount() {
        return movie_list.size();
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (movie_list.get(position).getMediatitle().length() > 8) {
            String name = movie_list.get(position).getMediatitle().substring(0, 8);
            Log.e("name", name);
            holder.movie_name.setText(name);
        } else
            holder.movie_name.setText(movie_list.get(position).getMediatitle());
        Picasso.with(context)
                .load(movie_list.get(position).getImage())
                .resize(200, 260)
                .placeholder(R.drawable.poster) // optional
                .error(R.drawable.poster)// optional
                .into(holder.img_android);

        if (!type.equals("recommended")) {
            final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    context, R.layout.spinner_item, rating_list) {
                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the item text color
                        tv.setTextColor(Color.parseColor("#FF7C7967"));
                        // Set the item background color
                        tv.setBackgroundColor(Color.parseColor("#96A3AD"));
                    } else if (position == 1) {
                        // Set the alternate item text color
                        tv.setTextColor(Color.parseColor("#FF657A86"));
                        // Set the alternate item background color
                        tv.setBackgroundColor(Color.parseColor("#F97777"));
                    } else if (position == 2) {
                        // Set the alternate item text color
                        tv.setTextColor(Color.parseColor("#FF657A86"));
                        // Set the alternate item background color
                        tv.setBackgroundColor(Color.parseColor("#5F64EF"));
                    } else if (position == 3) {
                        // Set the alternate item text color
                        tv.setTextColor(Color.parseColor("#FF657A86"));
                        // Set the alternate item background color
                        tv.setBackgroundColor(Color.parseColor("#54BF67"));
                    } else if (position == 4) {
                        // Set the alternate item text color
                        tv.setTextColor(Color.parseColor("#FF657A86"));
                        // Set the alternate item background color
                        tv.setBackgroundColor(Color.parseColor("#E564C1"));
                    } else if (position == 5) {
                        // Set the alternate item text color
                        tv.setTextColor(Color.parseColor("#FF657A86"));
                        // Set the alternate item background color
                        tv.setBackgroundColor(Color.parseColor("#D67B1A"));
                    }
                    return view;
                }
            };
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            holder.spinner_list.setAdapter(spinnerArrayAdapter);
            if (movie_list.get(position).getRating().equals("0"))
                holder.spinner_list.setSelection(0, false);
            else if (movie_list.get(position).getRating().equals("1"))
                holder.spinner_list.setSelection(1, false);
            else if (movie_list.get(position).getRating().equals("2"))
                holder.spinner_list.setSelection(2, false);
            else if (movie_list.get(position).getRating().equals("3"))
                holder.spinner_list.setSelection(3, false);
            else if (movie_list.get(position).getRating().equals("4"))
                holder.spinner_list.setSelection(4, false);
            else if (movie_list.get(position).getRating().equals("5"))
                holder.spinner_list.setSelection(5, false);
            final int color = Color.parseColor(movie_list.get(position).getRating_color());
            holder.spinner_list.setBackgroundColor(color);
            if (user_id.equals(new UserPref(context).getUserId())) {
                holder.spinner_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int posit, long l) {
                        Log.e("positionjjj "," " + posit);
                        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            Log.e("KITKAT device", "KITKAT device");
                            // only for gingerbread and newer versions
                        }
                        else
                            {
                                Log.e("positionjjjqqq "," " + posit);
                            if (first_position == posit || second_position == posit || third_position == posit || fourth_position == posit || fifth_position == posit || sixth_position == posit)
                            {
                                Log.e("position ksdakcbzxncz", String.valueOf(posit));
                                if (position == movie_list.size() - 1) {
                                    Log.e("position ", movie_list.size() - 1 + " " + position);
                                    first_position = 7;
                                    second_position = 7;
                                    third_position = 7;
                                    fourth_position = 7;
                                    fifth_position = 7;
                                    sixth_position = 7;
                                    return;
                                }

                                // some_position_for_device=posit;

                                Log.e("check", String.valueOf(check));
                                Log.e("rating position", String.valueOf(posit));
                                Log.e("position", String.valueOf(position));
                                sendToServer(posit, position);
                                setPosition(posit,holder);


                   /* else
                    {
                        first_position = 8;
                        second_position=8;
                        third_position=8;
                        fourth_position=8;
                        fifth_position=8;
                        sixth_position=8;

                    }*/
                                return;
                            }
                            else {
                                if (position == movie_list.size() - 1) {
                                    if (first_position == 7) {
                                        Log.e("new double  check", first_position + "third position " + third_position);
                                    }
                                    Log.e("position in else ", movie_list.size() - 1 + " " + position);
                                    first_position = 7;
                                    second_position = 7;
                                    third_position = 7;
                                    fourth_position = 7;
                                    fifth_position = 7;
                                    sixth_position = 7;
                                    return;
                                }
                               else if (first_position == 7)
                               {
                                    Log.e("new check", first_position + second_position + "third position " + third_position);
                                    sendToServer(posit, position);
                                    setPosition(posit,holder);


                                }
                                else
                                {
                                    if(posit==3 || posit==5)
                                    {
                                        Log.e("position nnnnnnin if ", " " + posit);
                                    }
                                    else {
                                        Log.e("position nnnnnn ", " " + posit);
                                        //  sendToServer(posit, position);
                                        setPosition(posit, holder);
                                    }
                                }
                            }
                            // UtilityPermission.showToast(context,adapterView.getSelectedItem().toString()+"Position "+ position);

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        Log.e("nothing", "nothing");

                    }
                });

            } else {
                holder.spinner_list.setEnabled(false);
            }
        } else {
            holder.added_text.setText(movie_list.get(position).getFirstname());
            holder.like_text.setText(movie_list.get(position).getLike());
            holder.dislike_text.setText(movie_list.get(position).getDislike());
            holder.like_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    HashMap<String, String> params = new HashMap<>();
                    params.put("media_id", movie_list.get(position).getId());
                    params.put("user_id", user_id);
                    params.put("category_id", movie_list.get(position).getCategoryid());
                    params.put("type", "like");
                    CallService.getInstance().passInfromation(api_listener, Constants.LIKE_DISLIKE_URL, params, true, "2", context);
                }
            });
            holder.dislike_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    HashMap<String, String> params = new HashMap<>();
                    params.put("media_id", movie_list.get(position).getId());
                    params.put("user_id", user_id);
                    params.put("category_id", movie_list.get(position).getCategoryid());
                    params.put("type", "dislike");
                    CallService.getInstance().passInfromation(api_listener, Constants.LIKE_DISLIKE_URL, params, true, "2", context);
                }
            });
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click", "on click");
                goItem(position);

            }
        });
        holder.itemView.setActivated(selectedItems.get(position, false));
    }

    private void setPosition(int posit, MyViewHolder holder) {
        if (posit == 0) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#96A3AD"));
        } else if (posit == 1) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#F97777"));
        } else if (posit == 2) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#5F64EF"));
        } else if (posit == 3) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#54BF67"));
        } else if (posit == 4) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#E564C1"));
        } else if (posit == 5) {
            holder.spinner_list.setBackgroundColor(Color.parseColor("#D67B1A"));
        }
    }

    private void sendToServer(int posit, int position) {
        HashMap<String, String> params = new HashMap<>();
        params.put("media_id", movie_list.get(position).getId());
        params.put("user_id", user_id);
        params.put("category_id", movie_list.get(position).getCategoryid());
        params.put("rating", String.valueOf(posit));
        CallService.getInstance().passInfromation(api_listener, Constants.MEDIA_RATING_URL, params, true, "1", context);
    }

    public void removeData(int position) {
        movie_list.remove(position);
        first_position = 8;
        second_position = 8;
        third_position = 8;
        fourth_position = 8;
        fifth_position = 8;
        sixth_position = 8;
        notifyItemRemoved(position);
    }

    private void goItem(int position) {
        adapter_listener.clickItem(position, "movie");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if (!type.equals("recommended")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.grid_layout, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recommended_row, parent, false);
        }
        return new MyViewHolder(itemView);
    }


    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        first_position = 8;
        second_position = 8;
        third_position = 8;
        fourth_position = 8;
        fifth_position = 8;
        sixth_position = 8;
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        first_position = 8;
        second_position = 8;
        third_position = 8;
        fourth_position = 8;
        fifth_position = 8;
        sixth_position = 8;
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            UtilityPermission.showToast(context, json_object.getString("message"));
                        } else {
                            UtilityPermission.showToast(context, json_object.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            JSONObject data_object = json_object.getJSONObject("data");
                            movie_list.get(pos).setLike(data_object.getString("is_like"));
                            movie_list.get(pos).setDislike(data_object.getString("dis_like"));
                            notifyDataSetChanged();
                        } else {
                            UtilityPermission.showToast(context, "Something went wrong");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };


}
