package com.android.webztalk.AllFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.NetworkCheck;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by ANUPAM TYAGI.
 */
public class ChangePasswordFragmnt extends Fragment implements View.OnClickListener{
    private FragmentListenerInterface listener;
    EditText oldpasswd, Newpaaswd, confrmPaaswd;
    private UserPref user_pref;
    private Button submit;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user_pref=new UserPref(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change_password, container, false);
        init(v);
        return v;
    }
    @Override
    public void onResume() {
        super.onResume();
        listener.showBackArrow(true);
    }
    public void init(View v) {
        oldpasswd = (EditText) v.findViewById(R.id.oldpassword);
        Newpaaswd = (EditText) v.findViewById(R.id.newpassword);
        confrmPaaswd = (EditText) v.findViewById(R.id.newconfirmpassword);
        submit = (Button) v.findViewById(R.id.update);
        submit.setOnClickListener(this);

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final MenuItem item1 = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        item1.setVisible(false);
    }
    @Override
    public void onClick(View v) {
        if(NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
            if (ValidatePassword()) {
                HashMap<String,String> params=new HashMap<String,String>();
                params.put("user_id",  user_pref.getUserId());
                params.put("old_p", oldpasswd.getText().toString());
                params.put("new_p",Newpaaswd.getText().toString());
                params.put("confirm_p", confrmPaaswd.getText().toString());
                CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_PASSWORD_URL,params,true,"1",getActivity());
            }
            //   getRemoteJSON(URL.change_phoneNumber + "&user_id=" + UserPreferences.GetUserID() + "&phone=" + NewNumValue);
        }
        else
        {
            UtilityPermission.showToast(getActivity(), Constants.NETWORK_STRING);
        }
    }

    /*
        @Override
        public void onClick(View v)
        {

            oldpasswdvalue = oldpasswd.getText().toString();
            Newpasswdvalue = Newpaaswd.getText().toString();
            NewConfrmpasswdvalue = confrmPaaswd.getText().toString();
            final String Newpasswd = Newpaaswd.getText().toString();
            if (oldpasswd.getText().toString().length() == 0 && oldpasswd.getText().toString().equalsIgnoreCase("")) {
                oldpasswd.setError("Invalid Email");
                Newpaaswd.setError(null);
            } else if (Newpaaswd.getText().toString().length() == 0 && Newpaaswd.getText().toString().equalsIgnoreCase("")) {
                oldpasswd.setError(null);
             o   Newpaaswd.setError("Invalid Password");
            } else if (confrmPaaswd.getText().toString().length() == 0 && confrmPaaswd.getText().toString().equalsIgnoreCase("")) {
                oldpasswd.setError(null);
                Newpaaswd.setError("Invalid  confirm Password");
            } else {
                Newpaaswd.setError(null);
                oldpasswd.setError(null);

                List<UserData> data = DBAdapter.getAllUserData();
                for (UserData dt : data) {
                    user_id = dt.get_id();
                    // Writing Contacts to log

                }

                getRemoteJSON(URL.change_passwd + "&user_id=" + user_id + "&old_p=" + oldpasswdvalue + "&new_p=" + Newpasswdvalue + "&confirm_p=" + NewConfrmpasswdvalue);
            }
        }*/
    public boolean ValidatePassword()
    {
        String old_password=oldpasswd.getText().toString();
        String new_password= Newpaaswd.getText().toString();
        if(old_password.length()>0) {
            if(new_password.length() >0)
            {
                if(confrmPaaswd.getText().toString().length()>0 && new_password.equals(confrmPaaswd.getText().toString()))
                {
                    return  true;
                }
                else
                {
                    confrmPaaswd.setError("Please Enter Confirm password");
                    return  false;
                }

            }
            else
            {
                Newpaaswd.setError("Please Enter New Password");
                return  false;
            }
        }
        else
            {
            oldpasswd.setError("Please Enter Password");
                return false;
            }

    }


    ApiResponseListener api_listener=new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response",response);
            if(response!=null)
            {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        //user_pref.setUserPassword(json.getString("password"));
                        UtilityPermission.showToast(getActivity(),"Password update successfully");
                    } else {
                        UtilityPermission.showToast(getActivity(),"You cant update your password");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
