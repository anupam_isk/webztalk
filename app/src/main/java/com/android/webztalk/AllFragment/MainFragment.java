package com.android.webztalk.AllFragment;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.FragmentInterface.onLoadMoreListener;
import com.android.webztalk.GsonResponse.CategoryResponse;
import com.android.webztalk.GsonResponse.SearchResponse;
import com.android.webztalk.Model.CategoryCount;
import com.android.webztalk.Model.Filter;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.adapter.GenureAdapter;
import com.android.webztalk.adapter.MovieAdapter;
import com.android.webztalk.adapter.MovieGridAdapter;
import com.android.webztalk.helper.AlertManger;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.GridSpacingItemDecoration;
import com.android.webztalk.helper.ImageTrans_CircleTransform;
import com.android.webztalk.helper.SimpleDividerDecoration;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    private RecyclerView recycler_view;
    private FragmentListenerInterface fragment_listener;
    private ImageView user_img,small_web;
    private Button movie, music, flag, book, tv, web, game, follower, following, plus, minus;
    private ArrayList<MediaObject> movie_list, search_movie_list,common_movie_list;
    private ArrayList<CategoryCount> category_list;
    private Spinner spinner;
    private GestureDetectorCompat gestureDetector;
    private String user_id, category_id = null, user_image, user_name,follower_count,following_count,bio_string;
    private TextView movie_name, name,bio;
    private MovieGridAdapter adapter;
    private MovieAdapter search_movie_adapter;
    private LinearLayout linear_row;
    private ArrayList<Filter> filter_list;
    private ArrayAdapter<String> dataAdapter;
    private  AlertDialog genre_popup;
    ActionMode actionMode;
  //  private ActionModeState actionModeState = ActionModeState.ITEM_NOT_CLICKED;
    private String term;
    private int start_value = 0;
    private int limit_value = 4;
    private boolean check = true,flag_check;
    // Initializing a String Array
    String[] plants = new String[]{
            "Filter By :",
            "Recently Added",
            "Rating",
            "Time Period",
            "In Common",
            "Genre"
    };
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        common_movie_list=new ArrayList<>();
        setHasOptionsMenu(true);
        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, plantsList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        movie_list = new ArrayList<>();
        if (getArguments() != null) {
            Bundle b = getArguments();
            user_id = b.getString("user_id");
            category_id = b.getString("category_id");
            user_image = b.getString("user_image");
            user_name = b.getString("user_name");
            follower_count=b.getString("follower_count");
            following_count=b.getString("following_count");
            bio_string=b.getString("bio");
            HashMap<String, String> params = new HashMap<>();
            params.put("category_id", category_id);
            params.put("user_id", user_id);
        /*  params.put("start",String.valueOf(start_value));
            params.put("offset",String.valueOf(limit_value));*/
            CallService.getInstance().passInfromation(api_listener, Constants.CATEGORY_URL, params, true, "1", getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        initializeUI(v);
        setListener();
        return v;
    }


    AdapterClickListener adapter_listener = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
        }
    };
    private void setListener() {
        movie.setOnClickListener(button_listener);
        music.setOnClickListener(button_listener);
        flag.setOnClickListener(button_listener);
        book.setOnClickListener(button_listener);
        tv.setOnClickListener(button_listener);
        web.setOnClickListener(button_listener);
        game.setOnClickListener(button_listener);
        plus.setOnClickListener(button_listener);
        minus.setOnClickListener(button_listener);
        follower.setOnClickListener(button_listener);
        following.setOnClickListener(button_listener);
        user_img.setOnClickListener(button_listener);
    }
    AdapterClickListener click_listner = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
            if (actionMode != null) {
                actionMode.finish();
            }
            if(type.equals("movie")) {
                Fragment fragment = new DetailFragment();
                Bundle b = new Bundle();
                b.putParcelable("movie_object", movie_list.get(position));
                fragment.setArguments(b);
                replaceFragment(fragment, "withbackstack");
            }
            else
            {
                searchBasedOnMediaType(filter_list.get(position).getId());
                genre_popup.dismiss();
                spinner.setSelection(0);
            }
        }
    };

    private void searchBasedOnMediaType(String id) {
        ArrayList<MediaObject> new_movie_list=new ArrayList<>();
        for(MediaObject m:common_movie_list)
        {
            if(id.equals(m.getMediatype()))
            new_movie_list.add(m);
        }
        if(new_movie_list.size()!=0) {
            movie_list.clear();
            movie_list.addAll(new_movie_list);
        }
        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
        recycler_view.setAdapter(adapter);
    }

    private void initializeUI(View v) {
        bio=(TextView) v.findViewById(R.id.bio);
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        user_img = (ImageView) v.findViewById(R.id.user_img);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        movie_name = (TextView) v.findViewById(R.id.movie_name);
        name = (TextView) v.findViewById(R.id.name);
        linear_row = (LinearLayout) v.findViewById(R.id.linear_row);
        movie = (Button) v.findViewById(R.id.movie);
        music = (Button) v.findViewById(R.id.music);
        flag = (Button) v.findViewById(R.id.flag);
        book = (Button) v.findViewById(R.id.book);
        tv = (Button) v.findViewById(R.id.tv);
        web = (Button) v.findViewById(R.id.web);
        game = (Button) v.findViewById(R.id.game);
        plus = (Button) v.findViewById(R.id.plus);
        minus = (Button) v.findViewById(R.id.minus);
        follower = (Button) v.findViewById(R.id.follower);
        following = (Button) v.findViewById(R.id.following);
        small_web=(ImageView) v.findViewById(R.id.small_web);
        name.setText(user_name);
        bio.setText((bio_string));
        follower.setText(follower_count+" "+"Followers");
        following.setText(following_count+" "+"Following");
        Picasso.with(getActivity())
                .load(user_image)
                .error(R.drawable.man)
                .placeholder(R.drawable.man)
                .resize(150, 150)
                .transform(new ImageTrans_CircleTransform(getActivity()))
                .into(user_img);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setNestedScrollingEnabled(false);
        int spanCount = 3; // 3 columns
        int spacing = 10; // 50px
        boolean includeEdge = true;
        recycler_view.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
       /* spinner.setSelection(3);
        spinner.setEnabled(false);*/
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String item = adapterView.getItemAtPosition(position).toString();
                if (item.equals("Filter By")) {
                } else if(item.equalsIgnoreCase("Recently Added")){

                    if(movie_list!=null) {
                        Collections.sort(movie_list, new CustomRecentlyAddedComparator());
                        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
                        recycler_view.setAdapter(adapter);
                    }
                }
                else if(item.equalsIgnoreCase("Rating"))
                {
                    if(movie_list!=null) {
                        Collections.sort(movie_list, new CustomComparator());
                        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
                        recycler_view.setAdapter(adapter);
                    }
                    //UtilityPermission.showToast(getActivity(), "Position " + position);
                }
                else if(item.equalsIgnoreCase("Time Period"))
                {
                    if(movie_list!=null) {
                        Collections.sort(movie_list, new CustomDateComparator());
                        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
                        recycler_view.setAdapter(adapter);
                    }
                }
                else if(item.equalsIgnoreCase("In Common"))
                {
                    getCommonList();
                }
                else if(item.equalsIgnoreCase("Genre"))
                {

                    if(filter_list!=null)
                    showGenurePopUp();
                    else
                       UtilityPermission.showToast(getActivity(),"There is no genure for this media !");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        recycler_view.addOnItemTouchListener(touch_listner);
        gestureDetector =
                new GestureDetectorCompat(getActivity(), new RecyclerViewDemoOnGestureListener());
        if (!user_id.equals(new UserPref(getActivity()).getUserId())) {
            linear_row.setVisibility(View.GONE);
        }

        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
        recycler_view.setAdapter(adapter);
/*        adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
               // Log.e("search movie list size",String.valueOf(search_movie_list.size()));
                if(movie_list!=null) {
                    adapter.addNull();

                    // adapter.notifyDataSetChanged();
                }
                start_value = movie_list.size();
                limit_value =  4;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("category_id", category_id);
                params.put("user_id", user_id);
                params.put("start",String.valueOf(start_value));
                params.put("offset",String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.CATEGORY_URL, params, false, "5", getActivity());
            }
        });*/

    }
    private void getCommonList() {
        ArrayList<MediaObject> new_movie_list=new ArrayList<>();
        for(MediaObject m:movie_list)
        {
            if(m.getIn_common().equals("1"))
                new_movie_list.add(m);
        }
        movie_list.clear();
        movie_list.addAll(new_movie_list);
        common_movie_list.clear();
        common_movie_list.addAll(new_movie_list);
        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
        recycler_view.setAdapter(adapter);
    }

    private void showGenurePopUp() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View recommended_view = factory.inflate(
                R.layout.showgenurelayout, null);
        genre_popup = new AlertDialog.Builder(getActivity()).create();
        genre_popup.setView(recommended_view);
        genre_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // recommended_pop.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_color)));
        ImageButton cross_button = (ImageButton) recommended_view.findViewById(R.id.back);
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genre_popup.dismiss();
            }
        });
        RecyclerView recycler_view = (RecyclerView) recommended_view.findViewById(R.id.recycler_view);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        GenureAdapter adapter = new GenureAdapter (filter_list, getActivity(), click_listner);
        recycler_view.setAdapter(adapter);
        genre_popup.show();
    }
      RecyclerView.OnItemTouchListener touch_listner = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };


    ActionMode.Callback action_mode = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.delete_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;

        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.e("actionclic","actionclic");
            switch (item.getItemId()) {
                case R.id.menu_item_delete:
                    Log.e("Log.e", "delete");
                    List<Integer> selectedItemPositions = adapter.getSelectedItems();
                    int currPos;
                    StringBuilder builder = new StringBuilder();
                    for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
                        currPos = selectedItemPositions.get(i);
                       /* Log.e("movie id",movie_list.get(currPos).getId());
                        Log.e("position ",String.valueOf(currPos));*/
                        builder.append(movie_list.get(currPos).getId() + ",");
                        //   RecyclerViewDemoApp.removeItemFromList(currPos);
                        adapter.removeData(currPos);
                        //
                    }
                    flag_check=true;
                    HashMap<String, String> params = new HashMap<>();
                    params.put("media_ids", builder.toString());
                    params.put("user_id", user_id);
                    CallService.getInstance().passInfromation(api_listener, Constants.DELETE_MY_WEB, params, true, "2", getActivity());
                    Log.e("movie id", builder.toString());
                    actionMode.finish();

                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            if(flag_check)
            {

            }
            else
            {
                clearList();
                getMediaFromServer(category_id);
            }

        }
    };

    @Override
    public void onPause() {
        super.onPause();
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setName();

    }

    private  void setName() {
        String movie_string = null;
        switch (category_id) {
            case "1":
                movie_string = "Movies";
                setColorAndName(R.color.movie_color,movie_string,R.drawable.small_movie);
                break;
            case "2":
                movie_string = "Music";
                setColorAndName(R.color.music_color,movie_string, R.drawable.small_music);
                break;
            case "3":
                movie_string = "Sports";
                setColorAndName(R.color.flag_color,movie_string, R.drawable.small_flag);
                break;
            case "4":
                movie_string = "Books";
                setColorAndName(R.color.book_color,movie_string, R.drawable.small_book);
                break;
            case "5":
                movie_string = "TV Shows";
                setColorAndName(R.color.tv_color,movie_string, R.drawable.small_tv);
                break;
            case "6":
                movie_string = "Apps";
                setColorAndName(R.color.app_color,movie_string, R.drawable.small_app);
                break;
            case "7":
                movie_string = "Games";
                setColorAndName(R.color.game_color,movie_string, R.drawable.small_game);
                break;
        }

    }

    private void setColorAndName(int movie_color, String movie_string, int small_movie) {
        movie_name.setText(movie_string);
        movie_name.setTextColor(getActivity().getResources().getColor(movie_color));
        small_web.setBackground(getResources().getDrawable(small_movie));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void replaceFragment(Fragment fragment, String type) {
        Log.e("type", type);
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();

    }


    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragment_listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fragment_listener.showBackArrow(false);
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("moview response", response);
            if (request_id.equals("1")) {
                flag_check=false;
                if (response != null) {
                    CategoryResponse category_response = new Gson().fromJson(response, CategoryResponse.class);
                    if (category_response.isStatus()) {
                        movie_list = category_response.getData().getCategory_data();
                        category_list = category_response.getData().getCategories();
                        filter_list=category_response.getData().getFilters();
                        if (movie_list != null && movie_list.size() > 0) {
                            adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_id, "main");
                            recycler_view.setAdapter(adapter);
                        } else {
                            UtilityPermission.showToast(getActivity(), "You have not added media yet!");
                        }
                        if(common_movie_list!=null) {
                            common_movie_list.clear();
                            common_movie_list.addAll(movie_list);
                        }
                        if (category_list != null) {
                            movie.setText(category_list.get(0).getCount());
                            music.setText(category_list.get(1).getCount());
                            flag.setText(category_list.get(2).getCount());
                            book.setText(category_list.get(3).getCount());
                            tv.setText(category_list.get(4).getCount());
                            web.setText(category_list.get(5).getCount());
                            game.setText(category_list.get(6).getCount());
                        }
                    }
                }
            }
            if (request_id.equals("2")) {

                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                            clearList();
                            getMediaFromServer(category_id);
                        } else {
                            UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("3")) {
                Log.e("user response ", response);
                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    ArrayList<MediaObject> new_list = null;
                    if (search_response.isStatus()) {
                        new_list = search_response.getData().getMedia();
                    }
                    if (new_list != null) {
                        search_movie_list.addAll(new_list);
                    }
                    // search_movie_adapter.setLoaded(true);
                    search_movie_adapter.notifyDataSetChanged();
                }
            }
            if (request_id.equals("4")) {
                Log.e("user response ", response);
                if (response != null) {
                    search_movie_list.remove(search_movie_list.size() - 1);
                    search_movie_adapter.notifyItemRemoved(search_movie_list.size());
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    ArrayList<MediaObject> new_list = null;
                    if (search_response.isStatus()) {
                        new_list = search_response.getData().getMedia();
                        if (new_list != null) {
                            search_movie_list.addAll(new_list);
                            search_movie_adapter.setLoaded(false);
                            search_movie_adapter.notifyDataSetChanged();
                        }
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no more data");
                    }

                }
            }

        }
    };
    View.OnClickListener button_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == movie) {
                category_id = "1";
                setName();
                clearList();
                getMediaFromServer("1");
            } else if (v == music) {
                category_id = "2";
                clearList();
                setName();
                getMediaFromServer("2");
            } else if (v == flag) {
                category_id = "3";
                setName();
                clearList();
                getMediaFromServer("3");
            } else if (v == book) {
                category_id = "4";
                setName();
                clearList();
                getMediaFromServer("4");
            } else if (v == tv) {
                category_id = "5";
                setName();
                clearList();
                getMediaFromServer("5");
            } else if (v == web) {
                category_id = "6";
                setName();
                clearList();
                getMediaFromServer("6");
            } else if (v == game) {
                category_id = "7";
                setName();
                clearList();
                getMediaFromServer("7");
            } else if (v == plus) {
                showPopUp();
            } else if (v == minus) {
                if (movie_list != null && movie_list.size() > 0) {
                    AlertManger.getAlert_instance().showAlert("Please long press on movie name for delete media !", getActivity());
                } else {
                    UtilityPermission.showToast(getActivity(), "Please  first do  add any movie!!");
                }
            } else if (v == user_img) {
                replaceFragment(new EditProfileFragment(), "withbackstack");
            } else if (v == follower) {
                showDialogFragment();
            } else if (v == following) {
                showDialogFragment();
            }
        }
    };

    private void showDialogFragment() {
        FollwerListFragment fragment = new FollwerListFragment();
        Bundle b = new Bundle();
        b.putString("user_id", user_id);
        fragment.setArguments(b);
        fragment.show(getActivity().getSupportFragmentManager(), "nn");
    }

    private void showPopUp() {
        search_movie_list = new ArrayList<>();
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View recommended_view = factory.inflate(
                R.layout.userpopup, null);
        final AlertDialog recommended_pop = new AlertDialog.Builder(getActivity()).create();
        recommended_pop.setView(recommended_view);
        recommended_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        recommended_pop.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_color)));
        // recommended_view.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));
        ImageButton cross_button = (ImageButton) recommended_view.findViewById(R.id.back);
        EditText search_text = (EditText) recommended_view.findViewById(R.id.search_text);
        search_text.setHint("Enter Media..");
        RecyclerView recycler_view = (RecyclerView) recommended_view.findViewById(R.id.recycler_view);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        search_movie_adapter = new MovieAdapter(search_movie_list, getActivity(), adapter_listener, recycler_view);
        recycler_view.setAdapter(search_movie_adapter);
        search_movie_adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("search movie list size", String.valueOf(search_movie_list.size()));
                if (search_movie_list != null) {
                    search_movie_list.add(null);
                    search_movie_adapter.notifyItemInserted(search_movie_list.size() - 1);
                    // adapter.notifyDataSetChanged();
                }
                start_value = search_movie_list.size();
                limit_value = 4; /*search_movie_list.size() +*/
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("term", term);
                params.put("category_id", category_id);
                params.put("start", String.valueOf(start_value));
                params.put("offset", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_MEDIA_URL, params, false, "4", getActivity());
            }
        });
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("charsequence", charSequence.toString());
                if (charSequence.length() == 0) {
                    clearSearchList();
                    recommended_pop.dismiss();
                    showPopUp();
                } else {
                    if (search_movie_list != null)
                        clearSearchList();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    // notification_list.clear();
                    getDataFromServer(charSequence.toString());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recommended_pop.dismiss();
            }
        });
        recommended_pop.show();
    }

    private void getDataFromServer(String content) {
        start_value = 0;
        limit_value = 5;
        term = content;
        HashMap<String, String> params = new HashMap<>();
        params.put("term", content);
        params.put("category_id", category_id);
        params.put("start", String.valueOf(start_value));
        params.put("offset", String.valueOf(limit_value));
        CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_MEDIA_URL, params, true, "3", getActivity());
    }
    private void clearList() {
        movie_list.clear();
        adapter.notifyDataSetChanged();
    }
    private void clearSearchList() {
        Log.e("search movie list", "search movie list");
        search_movie_list.clear();
        search_movie_adapter.notifyDataSetChanged();
    }
    private void getMediaFromServer(String category_id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("category_id", category_id);
        params.put("user_id", user_id);
        CallService.getInstance().passInfromation(api_listener, Constants.CATEGORY_URL, params, true, "1", getActivity());
    }

    /* @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         super.onOptionsItemSelected(item);
         if (item.getItemId() == R.id.delete_item) {
             removeItemFromList();
         }
         return true;
     }*/
    private void removeItemFromList() {
        int position = ((GridLayoutManager) recycler_view.getLayoutManager()).
                findFirstCompletelyVisibleItemPosition();
        // RecyclerViewDemoApp.removeItemFromList(position);
        adapter.removeData(position);
    }

    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
        String title = getString(R.string.selected_count, String.valueOf(adapter.getSelectedItemCount()));
        actionMode.setTitle(title);
    }
    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = recycler_view.findChildViewUnder(e.getX(), e.getY());
         /*   if (actionMode != null) {
                int idx = recycler_view.getChildPosition(view);
                myToggleSelection(idx);
                return super.onSingleTapConfirmed(e);
            }*/
            // onClick(view);
            return super.onSingleTapConfirmed(e);
        }
        public void onLongPress(MotionEvent e) {
            if (user_id.equals(new UserPref(getActivity()).getUserId())) {
                View view = recycler_view.findChildViewUnder(e.getX(), e.getY());
                if (actionMode != null) {
                    int idx = recycler_view.getChildPosition(view);
                    myToggleSelection(idx);
                    super.onLongPress(e);
                    return;
                }

                // Start the CAB using the ActionMode.Callback defined above
                actionMode = getActivity().startActionMode(action_mode);
                int idx = recycler_view.getChildPosition(view);
                myToggleSelection(idx);
                super.onLongPress(e);
            }
        }
    }

    public class CustomComparator implements Comparator<MediaObject> {
        @Override
        public int compare(MediaObject t1, MediaObject t2) {
            int returnVal = 0;
             int rating1=Integer.parseInt(t1.getRating());
             int rating2=Integer.parseInt(t2.getRating());
            if(rating2 < rating1){
                returnVal =  -1;
            }else if(rating2 > rating1){
                returnVal =  1;
            }else if(rating2 == rating1){
                returnVal =  0;
            }
            return returnVal;
        }
    }
    public class CustomDateComparator implements Comparator<MediaObject> {
        @Override
        public int compare(MediaObject o1, MediaObject o2) {
            return o2.getRelease_date().compareTo(o1.getRelease_date());
        }
    }
    public class CustomRecentlyAddedComparator implements Comparator<MediaObject> {
        @Override
        public int compare(MediaObject o1, MediaObject o2) {
            return o2.getRecently_id().compareTo(o1.getRecently_id());
        }
    }
}
