package com.android.webztalk.AllFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.GsonResponse.SearchResponse;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.adapter.MovieAdapter;
import com.android.webztalk.adapter.UserAdapter;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.SimpleDividerDecoration;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 8/10/2016.
 */
public class ShowFragment extends DialogFragment {
    private ImageButton back;
    private EditText search_text;
    private RecyclerView user_view, movie_view;
    private ArrayList<User> user_list;
    private ArrayList<MediaObject> movie_list;
    private MovieAdapter movie_adapter;
    private UserAdapter user_adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_list = new ArrayList<>();
        movie_list = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_fragment, container, false);
        initializeUi(view);
        return view;
    }

    private void initializeUi(View view) {
        back = (ImageButton) view.findViewById(R.id.back);
        search_text = (EditText) view.findViewById(R.id.search_text);
        user_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        movie_view = (RecyclerView) view.findViewById(R.id.movie_recycler);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        user_view.setLayoutManager(mLayoutManager);
        user_view.setItemAnimator(new DefaultItemAnimator());
        user_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        movie_view.setLayoutManager(mLayoutManager1);
        movie_view.setItemAnimator(new DefaultItemAnimator());
        movie_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        back.setOnClickListener(listener);
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 3) {
                    if (user_list != null)
                        user_list.clear();
                    if (movie_list != null)
                        movie_list.clear();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    // notification_list.clear();
                    getDataFromServer(editable.toString());

                } else {

                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        Log.e("height ", height + "");
        Log.e("width", width + "");
        // safety check
        if (getDialog() == null)
            return;

        int dialogWidth = width;// specify a value here
        int dialogHeight = height - 20; // specify a value here
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
        movie_adapter = new MovieAdapter(movie_list, getActivity(), adapter_listener, movie_view);
        movie_view.setAdapter(movie_adapter);
        user_adapter = new UserAdapter(user_list, getActivity(), adapter_listener);
        user_view.setAdapter(user_adapter);
    }

    private void getDataFromServer(String content) {
        HashMap<String, String> params = new HashMap<>();
        params.put("term", content);
        params.put("user_id", new UserPref(getActivity()).getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.SEARCHING_URL, params, true, "1", getActivity());
    }

    AdapterClickListener adapter_listener = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
            dismiss();
            if (type.equals("movie")) {
                Fragment fragment = new DetailFragment();
                Bundle b = new Bundle();
                b.putParcelable("movie_object", movie_list.get(position));
                fragment.setArguments(b);
                replaceFragment(fragment, "withoutbackstack");
            } else if (type.equals("user")) {
                Fragment fragment = new HomeFragment();
                Bundle b = new Bundle();
                b.putParcelable("user_object", user_list.get(position));
                fragment.setArguments(b);
                replaceFragment(fragment, "withoutbackstack");
            }

        }
    };
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == back) {
                dismiss();

            }
        }
    };

    private void replaceFragment(Fragment fragment, String type) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" search response", response);
            if (response != null) {
                SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                if (search_response.isStatus()) {
                    user_list = search_response.getData().getUser();
                    movie_list = search_response.getData().getMedia();
                    if (movie_list != null) {
                        movie_adapter = new MovieAdapter(movie_list, getActivity(), adapter_listener, movie_view);
                        movie_view.setAdapter(movie_adapter);
                    }
                    if (user_list != null) {
                        user_adapter = new UserAdapter(user_list, getActivity(), adapter_listener);
                        user_view.setAdapter(user_adapter);
                    }
                } else {
                    UtilityPermission.showToast(getActivity(), "There is no data");
                }

            }
        }
    };
}
