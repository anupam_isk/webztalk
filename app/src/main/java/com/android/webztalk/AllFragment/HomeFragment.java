package com.android.webztalk.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.HomeActivity;
import com.android.webztalk.Model.CategoryCount;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.ImageTrans_CircleTransform;
import com.android.webztalk.helper.UserPref;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private Button flag, tv_shows, movie, apps, games, music, books, follower, following;
    private FragmentListenerInterface fragment_listener;
    private ImageView user_img;
    private TextView name, bio_user;
    public String user_id, bio = "", density_screen;
    private float xdpi_float;
    private UserPref user_pref;
    String image_url = "", name_string, follower_count = "0", following_count = "0";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        density_screen = getDensityName(getActivity());
        printSecreenInfo();
        Log.e("density screen ", density_screen);
        user_pref = new UserPref(getActivity());
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    private static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

    void printSecreenInfo() {

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        Log.i(TAG, "density :" + metrics.density);

        // density interms of dpi
        Log.i(TAG, "D density :" + metrics.densityDpi);

        // horizontal pixel resolution
        Log.i(TAG, "width pix :" + metrics.widthPixels);

        // actual horizontal dpi
        Log.i(TAG, "xdpi :" + metrics.xdpi);
        xdpi_float = metrics.xdpi;

        // actual vertical dpi
        Log.i(TAG, "ydpi :" + metrics.ydpi);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        int layout;
        if (xdpi_float >= 300)
            layout = R.layout.fragment_demo_home;
        else
            layout = R.layout.fragment_demo_home_hdpi;
        View v = inflater.inflate(layout, container, false);
        initilaizeUI(v);
        setListener();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            Bundle b = getArguments();
            User user_object = b.getParcelable("user_object");
            user_id = user_object.getId();
            Log.e("category list",user_object.getCategories().size()+" size");
            ((HomeActivity) getActivity()).showAllViewBlack();
            // bio=user_pref.getAnotherUserName();
            following_count = user_object.getFollowing();
            follower_count = user_object.getFollower();
            bio = user_object.getUsername();
            if (user_object.getUserimage().equals(""))
                image_url = "http://ss/";
            else
                image_url = user_object.getUserimage();
            name_string = user_object.getFirstname();
            setCategoryCountWithList(user_object.getCategories());

        } else {
            user_id = user_pref.getUserId();
            follower_count = user_pref.getFollowerCount();
            following_count = user_pref.getFollowingCount();
            if (follower_count == null && following_count == null) {
                follower_count = "0";
                following_count = "0";
            }
            if (user_pref.getUserImage().equals(""))
                image_url = "http://ss/";
            else
                image_url = user_pref.getUserImage();
            name_string = user_pref.getUserName();
            bio = user_pref.getAnotherUserName();
            getCategoryCount();
        }
        setImage(image_url);
        name.setText(name_string);
        bio_user.setText(bio);
        follower.setText(follower_count + " " + "Followers");
        following.setText(following_count + " " + "Following");
    }
    private void setCategoryCountWithList(ArrayList<CategoryCount> categories) {
        movie.setText(categories.get(0).getCount());
        music.setText(categories.get(1).getCount());
        flag.setText(categories.get(2).getCount());
        books.setText(categories.get(3).getCount());
        tv_shows.setText(categories.get(4).getCount());
        apps.setText(categories.get(5).getCount());
        games.setText(categories.get(6).getCount());
    }

    private void setImage(String image_url) {
        Picasso.with(getActivity())
                .load(image_url)
                .error(R.drawable.man)
                .resize(150, 150)
                .transform(new ImageTrans_CircleTransform(getActivity()))
                .into(user_img);
    }

    private void getCategoryCount() {
        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("user_id", user_id);
        CallService.getInstance().passInfromation(response_listener, Constants.USER_WEB_URL, hmap, true, "1", getActivity());
    }

    private void setListener() {
        flag.setOnClickListener(listener);
        tv_shows.setOnClickListener(listener);
        movie.setOnClickListener(listener);
        apps.setOnClickListener(listener);
        games.setOnClickListener(listener);
        music.setOnClickListener(listener);
        books.setOnClickListener(listener);
        follower.setOnClickListener(listener);
        following.setOnClickListener(listener);
        user_img.setOnClickListener(listener);
    }

    private void setCategoryCount(JSONArray jsonArray) throws JSONException {
        movie.setText(jsonArray.getJSONObject(0).getString("count"));
        music.setText(jsonArray.getJSONObject(1).getString("count"));
        flag.setText(jsonArray.getJSONObject(2).getString("count"));
        books.setText(jsonArray.getJSONObject(3).getString("count"));
        tv_shows.setText(jsonArray.getJSONObject(4).getString("count"));
        apps.setText(jsonArray.getJSONObject(5).getString("count"));
        games.setText(jsonArray.getJSONObject(6).getString("count"));
    }

    private void initilaizeUI(View v) {
        flag = (Button) v.findViewById(R.id.bt1);
        tv_shows = (Button) v.findViewById(R.id.bt2);
        movie = (Button) v.findViewById(R.id.bt3);
        apps = (Button) v.findViewById(R.id.bt4);
        games = (Button) v.findViewById(R.id.bt5);
        music = (Button) v.findViewById(R.id.bt6);
        books = (Button) v.findViewById(R.id.bt7);
        follower = (Button) v.findViewById(R.id.follower);
        following = (Button) v.findViewById(R.id.following);
        bio_user = (TextView) v.findViewById(R.id.bio);
        name = (TextView) v.findViewById(R.id.name);
        user_img = (ImageView) v.findViewById(R.id.user_img);

    }

    View.OnClickListener listener = new View.OnClickListener() {
        Fragment fragment = null;

        @Override
        public void onClick(View view) {
            Bundle b = new Bundle();
            b.putString("user_id", user_id);
            b.putString("user_image", image_url);
            b.putString("user_name", name_string);
            b.putString("follower_count", follower_count);
            b.putString("following_count", following_count);
            b.putString("bio", bio);
            if (view == flag) {
                fragment = new MainFragment();
                b.putString("category_id", "3");
            } else if (view == tv_shows) {
                fragment = new MainFragment();
                b.putString("category_id", "5");
            } else if (view == movie) {
                fragment = new MainFragment();
                b.putString("category_id", "1");
            } else if (view == apps) {
                fragment = new MainFragment();
                b.putString("category_id", "6");
            } else if (view == games) {
                b.putString("category_id", "7");
                fragment = new MainFragment();
            } else if (view == music) {
                fragment = new MainFragment();
                b.putString("category_id", "2");
            } else if (view == books) {
                fragment = new MainFragment();
                b.putString("category_id", "4");
            } else if (view == follower) {

              /*  Intent i=new Intent(getActivity(), FollowActivity.class);
                startActivity(i);*/
                showDialogFragment();
            } else if (view == following) {
                showDialogFragment();
            } else if (view == user_img) {
                replaceFragment(new EditProfileFragment(), "withback");
            }
            if (view != follower && view != following && view != user_img) {
                fragment.setArguments(b);
                replaceFragment(fragment, "withoutbackstack");
            }
        }
    };

    private void showDialogFragment() {
        FollwerListFragment fragment = new FollwerListFragment();
        Bundle b = new Bundle();
        b.putString("user_id", user_id);
        fragment.setArguments(b);
        fragment.show(getActivity().getSupportFragmentManager(), "nn");
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragment_listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fragment_listener.showBackArrow(false);
    }

    private void replaceFragment(Fragment fragment, String type) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();
    }

    ApiResponseListener response_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("home response ",response);
            if (request_id.equals("1")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONObject json_object = json.getJSONObject("data").getJSONObject("user_detail");
                        setCategoryCount(json.getJSONObject("data").getJSONArray("categories"));
                        setUserCountandImage(json_object);

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    };
    private void setUserCountandImage(JSONObject json_object) throws JSONException {
        name.setText(json_object.getString("firstname"));
        bio_user.setText(json_object.getString("username"));
        follower.setText("Follower "+ json_object.getString("follower"));
        following.setText("Following "+json_object.getString("following"));
        setImage(json_object.getString("userimage"));
        user_pref.setUserName(json_object.getString("firstname"));
        user_pref.setAnotherUserName(json_object.getString("username"));
        user_pref.setUserImage(json_object.getString("userimage"));
        user_pref.setFollowerCount(json_object.getString("follower"));
        user_pref.setFollowingCount(json_object.getString("following"));
        getUserName();
    }
    private void getUserName() {
        image_url=user_pref.getUserImage();
        follower_count=user_pref.getFollowerCount();
        following_count=user_pref.getFollowingCount();
        bio=user_pref.getAnotherUserName();
        name_string=user_pref.getUserName();
    }
}
