package com.android.webztalk.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.ImageTrans_CircleTransform;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {
    private FragmentListenerInterface fragment_listener;
    private EditText name, phone, address, user_name, brief;
    private Button update;
    private String userChoosenTask, current;
    private ImageView user_img;
    private final int REQUEST_CAMERA = 1;
    private Bitmap bitmap = null;
    private final int SELECT_FILE = 2;
    private File myPath;
    private UserPref user_pref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_pref = new UserPref(getActivity());
        setHasOptionsMenu(true);
        Log.e("on create", "on create");
    }
    public EditProfileFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        initializeUI(v);
        setListener();
        setData();
        return v;
    }

    private void setListener() {
        user_img.setOnClickListener(listener);
        update.setOnClickListener(listener);
    }

    private void setData() {
        name.setText(user_pref.getUserName());
        address.setText(user_pref.getUserAddress());
        phone.setText(user_pref.getUserPhone());
        brief.setText(user_pref.getUserBio());
        user_name.setText(user_pref.getAnotherUserName());
        String image_url = "";
        if (user_pref.getUserImage().equals(""))
            image_url = "http://ss/";
        else
            image_url = user_pref.getUserImage();

        Log.e("image url", image_url);
        Picasso.with(getActivity())
                .load(image_url)
                .error(R.drawable.man)
                .resize(150, 150)
                .transform(new ImageTrans_CircleTransform(getActivity()))
                .into(user_img);

    }

    private void initializeUI(View v) {
        name = (EditText) v.findViewById(R.id.name);
        phone = (EditText) v.findViewById(R.id.phone);
        address = (EditText) v.findViewById(R.id.address);
        brief = (EditText) v.findViewById(R.id.brief);
        user_img = (ImageView) v.findViewById(R.id.user_img);
        update = (Button) v.findViewById(R.id.update);
        user_name = (EditText) v.findViewById(R.id.user_name);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragment_listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                boolean result = UtilityPermission.checkPermission(getActivity());
                if (items[position].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[position].equals("Choose from gallery")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[position].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(Intent.createChooser(i, "Select File"), SELECT_FILE);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final MenuItem item1 = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        item1.setVisible(false);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Log.e("request code", requestCode + "");
            if (requestCode == SELECT_FILE)
                onSelectGalleryImageResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectGalleryImageResult(Intent data) {
        if (data != null) {

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                String selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
                Log.e("Image  path", selectedPath);
                try {
                    myPath = new File(selectedPath);
                    if (myPath.exists()) {
                        Log.e("File name: ", myPath.getAbsolutePath());
                    }
                } catch (Exception e) {
                    Log.e("File not found : ", e.getMessage() + e);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            user_img.setImageBitmap(bitmap);
            // loadImageFromStorage(myPath);
        } else {
            Log.e("data is null", "data is null");
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        user_img.setImageBitmap(bitmap);
        createDirectory();
        // SaveToInternal(myPath);
        //  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (myPath == null) {
            Log.d("null",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        // bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
      /*  myPath= new File(Environment.getExternalStorageDirectory()+File.separator+"StaffTrader"+File.separator,
                System.currentTimeMillis() + ".png");*/
        FileOutputStream fo = null;
        try {
            //   myPath.createNewFile();
            fo = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fo);
            // fo.write(bytes.toByteArray());
            fo.close();
            if (myPath.exists()) {
                Log.e("File name: ", myPath.getAbsolutePath());
                Log.e("file name ", myPath.getName());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDirectory() {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("StaffTraderProfilePic", Context.MODE_PRIVATE);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e("directory is null", "directory is null");
            }
        }
        // Create imageDir
        // uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
        current = "profile.png";
        myPath = new File(directory.getPath() + File.separator + current);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UtilityPermission.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fragment_listener.showBackArrow(true);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == user_img) {
                selectImage();
            } else {
                if (validate()) {
                    updateProfile();
                }
                else
                {
                    UtilityPermission.showToast(getActivity(),"Please Enter Name.");
                }
            }

        }
    };
    private boolean validate() {
        if (name.getText().length() > 0) {
            if (user_name.getText().length() > 0)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }
    private void updateProfile() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_pref.getUserId());
        params.put("name", name.getText().toString());
        params.put("contact", phone.getText().toString());
        params.put("address", address.getText().toString());
        params.put("bio", brief.getText().toString());
        params.put("username", user_name.getText().toString());
        if (myPath == null)
            CallService.getInstance().passInfromation(api_listener, Constants.UPDATE_PROFILE_URL, params, true, "1", getActivity());
        else
            CallService.getInstance().passDataToMultiPart(api_listener, Constants.UPDATE_PROFILE_URL, params, true, myPath, "1", getActivity(), "img");
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (response != null) {
                Log.e("response ", response);
                try {
                    JSONObject json_object = new JSONObject(response);
                    if (json_object.getBoolean("status")) {
                        JSONObject user_object = json_object.getJSONObject("data");
                        user_pref.setUserName(user_object.getString("name"));
                        user_pref.setUserAddress(user_object.getString("useraddress"));
                        Log.e("addrss", user_object.getString("username") + " " + user_pref.getUserAddress());
                        user_pref.setUserBio(user_object.getString("userprofile"));
                        user_pref.setUserPhone(user_object.getString("userphone"));
                        user_pref.setUserImage(user_object.getString("userimage"));
                        user_pref.setAnotherUserName(user_object.getString("username"));
                        UtilityPermission.showToast(getActivity(), "Profile has been updated");
                    } else {
                        Log.e("status false", "status false");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
