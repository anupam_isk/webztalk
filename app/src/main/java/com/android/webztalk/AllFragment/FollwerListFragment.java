package com.android.webztalk.AllFragment;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.GsonResponse.FollowerResponse;
import com.android.webztalk.GsonResponse.FollowingResponse;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.adapter.UserAdapter;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.SimpleDividerDecoration;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollwerListFragment extends DialogFragment {
    private ImageButton back;
    private Button followbt, followingbt;
    private RecyclerView recycler_view;
    private int start_value = 0;
    private int limit_value = 10;
    private UserAdapter user_adapter;
    private ArrayList<User> user_list;
    private String user_id;

    public FollwerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle b = getArguments();
            user_id = b.getString("user_id");
        }
        user_list = new ArrayList<>();
        getFollower();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.fragment_follwer_list, container, false);
        initializeUI(v);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        Log.e("height ", height + "");
        Log.e("width", width + "");
        // safety check
        if (getDialog() == null)
            return;
        int dialogWidth = width - 10;// specify a value here
        int dialogHeight = height - 20; // specify a value here
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_color)));
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);

    }

    AdapterClickListener click_listener = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
            dismiss();
            Fragment fragment = new HomeFragment();
            Bundle b = new Bundle();
            b.putParcelable("user_object", user_list.get(position));
            fragment.setArguments(b);
            replaceFragment(fragment, "withoutbackstack");
        }
    };

    private void replaceFragment(Fragment fragment, String type) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();
    }

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        user_adapter = new UserAdapter(user_list, getActivity(), click_listener);
        recycler_view.setAdapter(user_adapter);
        followbt = (Button) v.findViewById(R.id.followbt);
        followingbt = (Button) v.findViewById(R.id.followingbt);
        showFollower();
        followbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_list != null) {
                    user_list.clear();
                    user_adapter.notifyDataSetChanged();
                }
                showFollower();
                getFollower();
            }
        });
        followingbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_list != null) {
                    user_list.clear();
                    user_adapter.notifyDataSetChanged();
                }
                followingbt.setBackgroundColor(getResources().getColor(R.color.transparent_color));
                followbt.setBackgroundColor(getResources().getColor(R.color.black_color));
                getFollowing();

            }
        });
        back = (ImageButton) v.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void showFollower() {
        followbt.setBackgroundColor(getResources().getColor(R.color.transparent_color));
        followingbt.setBackgroundColor(getResources().getColor(R.color.black_color));
    }

    private void getFollowing() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("start", String.valueOf(start_value));
        params.put("offset", String.valueOf(limit_value));
        CallService.getInstance().passInfromation(api_listener, Constants.FOLLOWING_USER_URL, params, true, "2", getActivity());
    }

    private void getFollower() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("start", String.valueOf(start_value));
        params.put("offset", String.valueOf(limit_value));
        CallService.getInstance().passInfromation(api_listener, Constants.FOLLOWER_USER_URL, params, true, "1", getActivity());
    }
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response user", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    FollowerResponse follower_response = new Gson().fromJson(response, FollowerResponse.class);
                    if (follower_response.isStatus()) {
                        user_list = follower_response.getData().getUser();
                        if (user_list != null) {
                            user_adapter = new UserAdapter(user_list, getActivity(), click_listener);
                            recycler_view.setAdapter(user_adapter);
                        }
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no data");
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    FollowingResponse follower_response = new Gson().fromJson(response, FollowingResponse.class);
                    if (follower_response.isStatus()) {

                        user_list = follower_response.getData().getUser();
                        if (user_list != null) {
                            user_adapter = new UserAdapter(user_list, getActivity(), click_listener);
                            recycler_view.setAdapter(user_adapter);
                        }
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no data");
                    }

                }
            }
        }

    };
}
