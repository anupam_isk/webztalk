package com.android.webztalk.AllFragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.FragmentInterface.onLoadMoreListener;
import com.android.webztalk.GsonResponse.SearchResponse;
import com.android.webztalk.HomeActivity;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.Model.User;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.adapter.MovieAdapter;
import com.android.webztalk.adapter.UserSearchAdapter;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.SimpleDividerDecoration;
import com.android.webztalk.helper.SpaceItemDecoration;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.PlusShare;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {
    public static final String API_KEY = "AIzaSyABhmoagd5d0i5_fm9p1dsPktYFVS6jJ3k";
    public static final String VIDEO_ID = "_oEA18Y8gM0";
    private FragmentListenerInterface fragment_listener;
    private ImageView movie_image;
    private TextView movie_name, release_date, awesome, web, recommend_web, share_movie, related_movie;
    private static View view;
    private MediaObject media;
    private UserSearchAdapter adapter;
    private MovieAdapter movie_adapter;
    private String term;
    private Button facebook, twitter, linkedin, pinterest, gmail;
    private ArrayList<User> user_list;
    private ArrayList<MediaObject> horizonatl_movie_list;
    private RecyclerView recycler_view;
    private int start_value = 0;
    private int limit_value = 5;
    private ShareDialog shareDialog;
    private YouTubePlayerSupportFragment youTubePlayerFragment;
    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        horizonatl_movie_list = new ArrayList<>();
        shareDialog = new ShareDialog(this); // intialize facebook shareDialog.
        if (getArguments() != null) {
            Bundle b = getArguments();
            media = b.getParcelable("movie_object");
            HashMap<String, String> params = new HashMap<>();
            params.put("category_id", media.getCategoryid());
            params.put("media_type", media.getMediatype());
            CallService.getInstance().passInfromation(api_listener, Constants.RELATED_MOVIES_URL, params, true, "4", getActivity());
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (media != null) {
            Picasso.with(getActivity())
                    .load(media.getImage())
                    .placeholder(R.drawable.poster) // optional
                    .error(R.drawable.poster)// optional
                    .into(movie_image);
            movie_name.setText(media.getMediatitle());
            release_date.setText("release date :" + media.getRelease_date());
            awesome.setText(media.getWord());
            Log.e("color", media.getColor());
            int color = Color.parseColor(media.getColor());
            awesome.setBackgroundColor(color);
            setName(media.getCategoryid());
            Log.e("name", media.getMediatitle());
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_detail, container, false);
        } catch (InflateException e) {
    /* map is already there, just return view as it is */
        }
        initailizeUI(view);
        setListener();
            if (youTubePlayerFragment == null) {
                Log.e("you tube is null", "you tube is null");
                youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.add(R.id.youtube_fragment, youTubePlayerFragment).commit();
            }
            /** Initializing YouTube player view **/
         /*   youTubePlayerFragment = (YouTubePlayerSupportFragment) getChildFragmentManager()
                    .findFragmentById(R.id.youtube_fragment);*/
            youTubePlayerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean wasRestored) {
                    if (!wasRestored) {
                        try {
                            if (media != null)
                                youTubePlayer.cueVideo(media.getYoutube_url());
                            else
                                youTubePlayer.cueVideo(VIDEO_ID);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                    // TODO Auto-generated method stub
                    Toast.makeText(getActivity(), " errorMessage", Toast.LENGTH_LONG).show();
                }
            });
        return view;
    }

    private void setListener() {
        web.setOnClickListener(click_listener);
        recommend_web.setOnClickListener(click_listener);
        facebook.setOnClickListener(click_listener);
        twitter.setOnClickListener(click_listener);
        linkedin.setOnClickListener(click_listener);
        pinterest.setOnClickListener(click_listener);
        gmail.setOnClickListener(click_listener);
    }

    private void setName(String category_id) {
        String movie_string = null, related_string = null;
        switch (category_id) {
            case "1":
                movie_string = "Shared Movie";
                related_string = "Related Movies";
                break;
            case "2":
                movie_string = "Shared Music";
                related_string = "Related Musics";
                break;
            case "3":
                movie_string = "Shared Sport";
                related_string = "Related Sports";
                break;
            case "4":
                movie_string = "Shared Book";
                related_string = "Related Books";
                break;
            case "5":
                movie_string = "Shared TV Shoe";
                related_string = "Related TV Shoes";
                break;
            case "6":
                movie_string = "Shared App";
                related_string = "Related Apps";
                break;
            case "7":
                movie_string = "Shared Game";
                related_string = "Related Games";
                break;
        }
        share_movie.setText(movie_string);
        related_movie.setText(related_string);

    }

    private void initailizeUI(View view) {
        movie_image = (ImageView) view.findViewById(R.id.movie_image);
        movie_name = (TextView) view.findViewById(R.id.movie_name);
        release_date = (TextView) view.findViewById(R.id.release_date);
        awesome = (TextView) view.findViewById(R.id.awesome);
        share_movie = (TextView) view.findViewById(R.id.share_movie);
        related_movie = (TextView) view.findViewById(R.id.related_movie);
        facebook = (Button) view.findViewById(R.id.facebook);
        twitter = (Button) view.findViewById(R.id.twitter);
        linkedin = (Button) view.findViewById(R.id.linkedin);
        pinterest = (Button) view.findViewById(R.id.pinterest);
        gmail = (Button) view.findViewById(R.id.gmail);
        web = (TextView) view.findViewById(R.id.web);
        recommend_web = (TextView) view.findViewById(R.id.recommend_web);
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new SpaceItemDecoration(20));
        movie_adapter = new MovieAdapter(horizonatl_movie_list, getActivity(), adapter_listener, recycler_view, "horizontal");
        recycler_view.setAdapter(movie_adapter);
    }
    private void replaceFragment(Fragment fragment, String type) {
        Log.e("type", type);
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragment_listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        fragment_listener.showBackArrow(true);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == web) {
                if (media != null) {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("user_id", new UserPref(getActivity()).getUserId());
                    params.put("media_id", media.getId());
                    params.put("category_id", media.getCategoryid());
                    CallService.getInstance().passInfromation(api_listener, Constants.ADD__MYWEB_URL, params, true, "1", getActivity());
                }
            } else if (view == recommend_web) {
                if (media != null) {
                    showPopUp();
                }


            } else if (view == facebook) {
                showToast("Please wait...");
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(media.getMediatitle())
                            .setImageUrl(Uri.parse(media.getImage()))
                            .setContentDescription(
                                    media.getMediadescription())
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=" + getActivity().getPackageName()))
                            .build();

                    shareDialog.show(linkContent); // Show facebook ShareDialog
                }

            } else if (view == twitter) {
                showToast("Please wait...");
                //  File myImageFile = new File("/storage/emulated/0/attachment.jpg");
                // Log.e("path",f.getAbsolutePath());
                TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                        .text(media.getMediadescription());
                builder.show();
            } else if (view == linkedin) {
                showToast("Please wait...");
                String text1 = "http://iskdevsrvr.com/webztalk/";
                Intent linkedinIntent = new Intent(Intent.ACTION_SEND);
                linkedinIntent.setType("text/plain");
                linkedinIntent.putExtra(Intent.EXTRA_TEXT, text1);
                boolean linkedinAppFound = false;
                List<ResolveInfo> matches2 = getActivity().getPackageManager()
                        .queryIntentActivities(linkedinIntent, 0);
                for (ResolveInfo info : matches2) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith(
                            "com.linkedin")) {
                        linkedinIntent.setPackage(info.activityInfo.packageName);
                        linkedinAppFound = true;
                        break;
                    }
                }
                if (linkedinAppFound) {
                    startActivity(linkedinIntent);

                } else {
                    Toast.makeText(getActivity(), "LinkedIn app not Insatlled in your mobile", Toast.LENGTH_SHORT).show();
                }

            } else if (view == gmail) {
                showToast("Please wait...");
                Log.e("gmail", "gmail");
                Intent shareIntent = new PlusShare.Builder(getActivity())
                        .setType("text/plain")
                        .setText(media.getMediadescription())
                        .setContentUrl(Uri.parse("http://iskdevsrvr.com/webztalk/"))
                        .getIntent();
                startActivityForResult(shareIntent, 0);
            } else if (view == pinterest) {
                showToast("Please wait...");
                shareOnPinterest(media.getImage(), media.getMediadescription(), media.getMediatitle());

            }

        }
    };

    private void showToast(String s) {
        UtilityPermission.showToast(getActivity(), s);
    }


    private void shareOnPinterest(String image, String mediadescription, String mediatitle) {
        String shareUrl = "http://www.pinterest.com/pin/create/button/?media=" + image + "&title=" + mediatitle + "&description=" + mediadescription;
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(shareUrl));
        startActivity(myIntent);
    }


    private void showPopUp() {
        user_list = new ArrayList<>();
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View recommended_view = factory.inflate(
                R.layout.userpopup, null);
        final AlertDialog recommended_pop = new AlertDialog.Builder(getActivity()).create();
        recommended_pop.setView(recommended_view);
        recommended_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        recommended_pop.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_color)));
        ImageButton cross_button = (ImageButton) recommended_view.findViewById(R.id.back);
        EditText search_text = (EditText) recommended_view.findViewById(R.id.search_text);
        RecyclerView recycler_view = (RecyclerView) recommended_view.findViewById(R.id.recycler_view);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        adapter = new UserSearchAdapter(user_list, getActivity(), adapter_listener, media.getId(), media.getCategoryid(), recycler_view);
        recycler_view.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
                user_list.add(null);
                adapter.notifyItemInserted(user_list.size() - 1);
                start_value = user_list.size() - 1;
                limit_value = (user_list.size() - 1) + 6;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("term", term);
                params.put("user_id", new UserPref(getActivity()).getUserId());
                params.put("start", String.valueOf(start_value));
                params.put("offset", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.RECOMMENDED_SEARCHING_URL, params, true, "3", getActivity());
            }
        });
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 3) {
                    if (user_list != null)
                        user_list.clear();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    // notification_list.clear();
                    getDataFromServer(editable.toString());

                } else {

                }
            }
        });
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recommended_pop.dismiss();
            }
        });
        recommended_pop.show();

    }


    AdapterClickListener adapter_listener = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
            if (type.equals("horizontal")) {
                Fragment fragment = new DetailFragment();
                Bundle b = new Bundle();
                b.putParcelable("movie_object", horizonatl_movie_list.get(position));
                fragment.setArguments(b);
                replaceFragment(fragment);
            }

        }
    };

    /*   @Override
       public void onActivityResult(int requestCode, int resultCode, Intent data) {
           // Add this line to your existing onActivityResult() method
           LISessionManager.getInstance(getActivity().getApplicationContext()).onActivityResult(getActivity(), requestCode, resultCode, data);
           Log.e("linkedin","linkedin result ");
       }*/
    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.replace(R.id.container, fragment).commit();
    }


    private void getDataFromServer(String content) {
        start_value = 0;
        limit_value = 6;
        term = content;
        HashMap<String, String> params = new HashMap<>();
        params.put("term", content);
        params.put("user_id", new UserPref(getActivity()).getUserId());
        params.put("start", String.valueOf(start_value));
        params.put("offset", String.valueOf(limit_value));
        CallService.getInstance().passInfromation(api_listener, Constants.RECOMMENDED_SEARCHING_URL, params, true, "2", getActivity());
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                try {
                    JSONObject json_object = new JSONObject(response);
                    if (json_object.getBoolean("status")) {
                        UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                    } else {
                        UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (request_id.equals("2")) {
                Log.e("user response ", response);
                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    ArrayList<User> new_list = null;
                    if (search_response.isStatus()) {
                        new_list = search_response.getData().getUser();
                    }
                    if (new_list != null) {
                        user_list.addAll(new_list);
                    }
                    // adapter.setLoaded(true);
                    adapter.notifyDataSetChanged();
                }
            }
            if (request_id.equals("3")) {
                Log.e("user response ", response);
                if (response != null) {
                    user_list.remove(user_list.size() - 1);
                    adapter.notifyItemRemoved(user_list.size());
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    ArrayList<User> new_list = null;
                    if (search_response.isStatus()) {
                        new_list = search_response.getData().getUser();
                        if (new_list != null) {
                            user_list.addAll(new_list);
                            adapter.setLoaded(false);
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no more data");
                    }
                }
            }
            if (request_id.equals("4")) {
                Log.e("movie response ", response);
                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    ArrayList<MediaObject> new_list = null;
                    if (search_response.isStatus()) {
                        new_list = search_response.getData().getMedia();
                    }
                    if (new_list != null) {
                        horizonatl_movie_list.addAll(new_list);
                    }
                    // search_movie_adapter.setLoaded(true);
                    movie_adapter.notifyDataSetChanged();
                }
            }

        }
    };
}
