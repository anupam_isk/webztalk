package com.android.webztalk.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.webztalk.FragmentInterface.AdapterClickListener;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.GsonResponse.CategoryResponse;
import com.android.webztalk.Model.CategoryCount;
import com.android.webztalk.Model.Filter;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.R;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.adapter.MovieGridAdapter;
import com.android.webztalk.helper.AlertManger;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.GridSpacingItemDecoration;
import com.android.webztalk.helper.ImageTrans_CircleTransform;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecommendedFragment extends Fragment {
    private RecyclerView recycler_view;
    private FragmentListenerInterface fragment_listener;
    private ImageView user_img,small_web;
    private Spinner spinner;
    private ArrayList<MediaObject> movie_list, common_movie_list;
    GestureDetectorCompat gestureDetector;
    private MovieGridAdapter adapter;
    ArrayAdapter<String> dataAdapter;
    ActionMode actionMode;
    private String image_url;
    private ArrayList<CategoryCount> category_list;
    private TextView movie_name, name, bio;
    private UserPref user_pref;
    private ArrayList<Filter> filter_list;
    private Button movie, music, flag, book, tv, web, game, follower, following, plus, minus;

    public RecommendedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        common_movie_list = new ArrayList<>();
        filter_list = new ArrayList<>();
        user_pref = new UserPref(getActivity());
        setHasOptionsMenu(true);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_pref.getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.RECOMMENDED_WEB_URL, params, true, "1", getActivity());
        movie_list = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        initializeUI(v);
        setListener();
        return v;
    }

    private void setListener() {
        movie.setOnClickListener(button_listener);
        music.setOnClickListener(button_listener);
        flag.setOnClickListener(button_listener);
        book.setOnClickListener(button_listener);
        tv.setOnClickListener(button_listener);
        web.setOnClickListener(button_listener);
        game.setOnClickListener(button_listener);
        minus.setOnClickListener(button_listener);
        follower.setOnClickListener(button_listener);
        following.setOnClickListener(button_listener);
    }

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        user_img = (ImageView) v.findViewById(R.id.user_img);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        movie_name = (TextView) v.findViewById(R.id.movie_name);
        name = (TextView) v.findViewById(R.id.name);
        movie = (Button) v.findViewById(R.id.movie);
        music = (Button) v.findViewById(R.id.music);
        flag = (Button) v.findViewById(R.id.flag);
        book = (Button) v.findViewById(R.id.book);
        tv = (Button) v.findViewById(R.id.tv);
        web = (Button) v.findViewById(R.id.web);
        game = (Button) v.findViewById(R.id.game);
        plus = (Button) v.findViewById(R.id.plus);
        minus = (Button) v.findViewById(R.id.minus);
        follower = (Button) v.findViewById(R.id.follower);
        following = (Button) v.findViewById(R.id.following);
        small_web = (ImageView) v.findViewById(R.id.small_web);
        bio = (TextView) v.findViewById(R.id.bio);
        plus.setVisibility(View.GONE);
        small_web.setVisibility(View.GONE);
        name.setText(user_pref.getUserName());
        bio.setText(user_pref.getAnotherUserName());
        follower.setText(user_pref.getFollowerCount() + " " + "Followers");
        following.setText(user_pref.getFollowingCount() + " " + "Following");
        if (user_pref.getUserImage().equals(""))
            image_url = "http://ss/";
        else
            image_url = user_pref.getUserImage();
        Picasso.with(getActivity())
                .load(image_url)
                .error(R.drawable.man)
                .resize(150, 150)
                .transform(new ImageTrans_CircleTransform(getActivity()))
                .into(user_img);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        recycler_view.setLayoutManager(layoutManager);
        recycler_view.setNestedScrollingEnabled(false);
        int spanCount = 3; // 3 columns
        int spacing = 15; // 50px
        boolean includeEdge = true;
        recycler_view.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
       /* spinner.setSelection(3);
        spinner.setEnabled(false);*/
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //   String item = adapterView.getItemAtPosition(position).toString();
                String item = filter_list.get(position).getCategoryname();
                if (item.equals("Filter By")) {

                } else {
                    getFilterDataById(filter_list.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        recycler_view.addOnItemTouchListener(touch_listner);
        gestureDetector = new GestureDetectorCompat(getActivity(), new RecyclerViewDemoOnGestureListener());
        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_pref.getUserId(), "recommended");
        recycler_view.setAdapter(adapter);
     /*   adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
               // Log.e("search movie list size",String.valueOf(search_movie_list.size()));
                if(movie_list!=null) {
                    adapter.addNull();

                    // adapter.notifyDataSetChanged();
                }
                start_value = movie_list.size();
                limit_value =  4;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("category_id", category_id);
                params.put("user_id", user_id);
                params.put("start",String.valueOf(start_value));
                params.put("offset",String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.CATEGORY_URL, params, false, "5", getActivity());
            }
        });*/
    }

    private void getFilterDataById(String id) {
        ArrayList<MediaObject> new_movie_list = new ArrayList<>();
        for (MediaObject m : common_movie_list) {
            if (id.equals(m.getCategoryid()))
                new_movie_list.add(m);
        }
        movie_list.clear();
        movie_list.addAll(new_movie_list);
        adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_pref.getUserId(), "recommended");
        recycler_view.setAdapter(adapter);
        setName(id);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    AdapterClickListener click_listner = new AdapterClickListener() {
        @Override
        public void clickItem(int position, String type) {
            if (actionMode != null) {
                actionMode.finish();
            }
            Fragment fragment = new DetailFragment();
            Bundle b = new Bundle();
            b.putParcelable("movie_object", movie_list.get(position));
            fragment.setArguments(b);
            replaceFragment(fragment,"withbackstack");
        }
    };

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        fragment_transaction.replace(R.id.container, fragment).commit();
    }
    private void replaceFragment(Fragment fragment, String type) {
        Log.e("type", type);
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();

    }
    ActionMode.Callback action_mode = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.delete_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_item_delete:
                    List<Integer> selectedItemPositions = adapter.getSelectedItems();
                    int currPos;
                    StringBuilder builder = new StringBuilder();
                    for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
                        currPos = selectedItemPositions.get(i);
                        builder.append(movie_list.get(currPos).getId() + ",");
                        //   RecyclerViewDemoApp.removeItemFromList(currPos);
                        adapter.removeData(currPos);
                        HashMap<String, String> params = new HashMap<>();
                        params.put("media_ids", builder.toString());
                        params.put("user_id", user_pref.getUserId());
                        CallService.getInstance().passInfromation(api_listener, Constants.DELETE_RECOMMENDED_URL, params, true, "2", getActivity());
                        //
                    }
                    actionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            adapter.clearSelections();
        }
    };

    private void setName(String category_id) {
        String movie_string = null;
        switch (category_id) {
            case "1":
                movie_string = "Movies";
                setColorAndName(R.color.movie_color, movie_string);
                break;
            case "2":
                movie_string = "Music";
                setColorAndName(R.color.music_color, movie_string);
                break;
            case "3":
                movie_string = "Sports";
                setColorAndName(R.color.flag_color, movie_string);
                break;
            case "4":
                movie_string = "Books";
                setColorAndName(R.color.book_color, movie_string);
                break;
            case "5":
                movie_string = "TV Shows";
                setColorAndName(R.color.tv_color, movie_string);
                break;
            case "6":
                movie_string = "Apps";
                setColorAndName(R.color.app_color, movie_string);
                break;
            case "7":
                movie_string = "Games";
                setColorAndName(R.color.game_color, movie_string);
                break;
        }

    }

    private void setColorAndName(int movie_color, String movie_string) {
        movie_name.setText(movie_string);
        movie_name.setTextColor(getActivity().getResources().getColor(movie_color));
    }

    RecyclerView.OnItemTouchListener touch_listner = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };

    View.OnClickListener button_listener = new View.OnClickListener() {
        Fragment fragment = null;

        @Override
        public void onClick(View v) {
            Bundle b = new Bundle();
            b.putString("user_id", user_pref.getUserId());
            b.putString("user_image", user_pref.getUserImage());
            b.putString("user_name", user_pref.getUserName());
            b.putString("bio", user_pref.getAnotherUserName());
            b.putString("follower_count", user_pref.getFollowerCount());
            b.putString("following_count", user_pref.getFollowingCount());
            if (v == movie) {
                fragment = new MainFragment();
                b.putString("category_id", "1");

            } else if (v == music) {
                fragment = new MainFragment();
                b.putString("category_id", "2");
            } else if (v == flag) {
                fragment = new MainFragment();
                b.putString("category_id", "3");
            } else if (v == book) {
                fragment = new MainFragment();
                b.putString("category_id", "4");
            } else if (v == tv) {
                fragment = new MainFragment();
                b.putString("category_id", "5");
            } else if (v == web) {
                fragment = new MainFragment();
                b.putString("category_id", "6");
            } else if (v == game) {
                fragment = new MainFragment();
                b.putString("category_id", "7");
            } else if (v == minus) {
                if (movie_list != null && movie_list.size() > 0) {
                    AlertManger.getAlert_instance().showAlert("Please long press on movie name for delete media !", getActivity());
                } else {
                    UtilityPermission.showToast(getActivity(), "Please  first do  add any movie!!");
                }
            } else if (v == follower) {
                showDialogFragment();
            } else if (v == following) {
                showDialogFragment();
            }
            if (v != follower && v != following && v != minus) {
                fragment.setArguments(b);
                replaceFragment(fragment);
            }
        }
    };

    private void showDialogFragment() {
        FollwerListFragment fragment = new FollwerListFragment();
        Bundle b = new Bundle();
        b.putString("user_id", user_pref.getUserId());
        fragment.setArguments(b);
        fragment.show(getActivity().getSupportFragmentManager(), "nn");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.login, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragment_listener = (FragmentListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fragment_listener.showBackArrow(false);
    }

    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = recycler_view.findChildViewUnder(e.getX(), e.getY());
         /*   if (actionMode != null) {
                int idx = recycler_view.getChildPosition(view);
                myToggleSelection(idx);
                return super.onSingleTapConfirmed(e);
            }*/
            // onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = recycler_view.findChildViewUnder(e.getX(), e.getY());
            if (actionMode != null) {
                int idx = recycler_view.getChildPosition(view);
                myToggleSelection(idx);
                super.onLongPress(e);
                return;
            }
            // Start the CAB using the ActionMode.Callback defined above
            actionMode = getActivity().startActionMode(action_mode);
            int idx = recycler_view.getChildPosition(view);
            myToggleSelection(idx);
            super.onLongPress(e);

        }
    }

    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
        String title = getString(R.string.selected_count, String.valueOf(adapter.getSelectedItemCount()));
        actionMode.setTitle(title);
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    CategoryResponse category_response = new Gson().fromJson(response, CategoryResponse.class);
                    if (category_response.isStatus()) {
                        movie_list = category_response.getData().getCategory_data();
                        category_list = category_response.getData().getCategories();
                        if (common_movie_list != null) {
                            common_movie_list.clear();
                            common_movie_list.addAll(movie_list);
                        }
                        ArrayList<Filter> new_filter_list = category_response.getData().getFilters();
                        Filter filter = new Filter();
                        filter.setCategoryname("Filter By");
                        filter.setCount("");
                        filter.setColor_code("#FFFFFF");
                        filter_list.add(filter);
                        filter_list.addAll(new_filter_list);
                        CustomAdapter customAdapter = new CustomAdapter(filter_list, getActivity());
                        spinner.setAdapter(customAdapter);
                        if (movie_list != null && movie_list.size() > 0) {
                            adapter = new MovieGridAdapter(movie_list, getActivity(), click_listner, user_pref.getUserId(), "recommended");
                            recycler_view.setAdapter(adapter);
                        } else {
                            UtilityPermission.showToast(getActivity(), "You have not added media yet!");
                        }
                        if (category_list != null) {
                            movie.setText(category_list.get(0).getCount());
                            music.setText(category_list.get(1).getCount());
                            flag.setText(category_list.get(2).getCount());
                            book.setText(category_list.get(3).getCount());
                            tv.setText(category_list.get(4).getCount());
                            web.setText(category_list.get(5).getCount());
                            game.setText(category_list.get(6).getCount());
                        }
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                        } else {
                            UtilityPermission.showToast(getActivity(), json_object.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    public class CustomAdapter extends BaseAdapter {
        LayoutInflater inflter;
        Context context;
        private ArrayList<Filter> filter_list;

        public CustomAdapter(ArrayList<Filter> filter_list, Context context) {
            this.filter_list = filter_list;
            this.context = context;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return filter_list.size();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.row_layout, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(filter_list.get(i).getCategoryname() + " - " + filter_list.get(i).getCount());
            name.setTextColor(getActivity().getResources().getColor(R.color.black_color));
            name.setBackgroundColor(Color.parseColor(filter_list.get(i).getColor_code()));
            return view;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }
    }
}
