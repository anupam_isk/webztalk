package com.android.webztalk.FragmentInterface;

/**
 * Created by Anupam  on 8/26/2016.
 */
public interface AdapterClickListener {
    void clickItem(int position,String type);
}
