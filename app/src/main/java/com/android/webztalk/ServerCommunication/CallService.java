package com.android.webztalk.ServerCommunication;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.webztalk.Application.ApplicationContextProvider;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.MultipartRequest;
import com.android.webztalk.helper.UtilityPermission;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anupam tyagi on 6/28/2016.
 */

// This class created for get a response from server

public class CallService {
    private ApiResponseListener listener;
    private HashMap<String,String> params;
    private boolean check;
    private String request_id;
    private static CallService instance=new CallService();
    private String url;
    private ProgressDialog progresDialog;
    private Context context;

    public static CallService getInstance()
    {
        return instance;
    }
    public void passInfromation(ApiResponseListener listener,String url,HashMap<String,String> params,boolean check,String request_id,Context context)
    {
        this.params=params;
        this.check=check;
        this.listener=listener;
        this.context=context;
        this.request_id=request_id;
        Log.e("params ",params.toString());
        this.url=url;
        getResponseFromServer();

    }

    public void passDataToMultiPart(final ApiResponseListener listener, String url, HashMap<String,String> params, boolean check, File file, final String inner_request_id, final Context context, String content_type)
    {
        this.context=context;
        if(check) {
            progresDialog = new ProgressDialog(context);
            progresDialog.setMessage("Please Wait");
            progresDialog.setCancelable(true);
            progresDialog.show();
        }
        Log.e("params ",params.toString());
        Log.e("file url",file.getPath());
        MultipartRequest mr = new MultipartRequest(url, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showError(error);
            }
        }, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(progresDialog.isShowing())
                    progresDialog.dismiss();

                listener.getResponse(response,inner_request_id);
            }
        }, file, params,content_type)
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("token","AFcWxV21C7fd0v3bYYYRCpSSRl31AcsAW04eK1GU5HZCj8LnCCFBd");
                //     header.put("Content-Type", "application/json");
                return header;
            }
        };

        //  DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2
        mr.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ApplicationContextProvider.getInstance().addToRequestQueue(mr," tag_json_obj");
    }
    public  void passCategoryinformation(ApiResponseListener listener,String url,boolean check,String request_id,Context context)
    {
        this.check=check;
        this.listener=listener;
        this.context=context;
        this.request_id=request_id;
        this.url=url;
        getCategoryDataFromServer();
    }
    private void showError(VolleyError error) {
        if(progresDialog.isShowing())
            progresDialog.dismiss();
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
          UtilityPermission.showToast(context,"Time out error ");
        } else if (error instanceof AuthFailureError) {
           UtilityPermission.showToast(context, "Authantication faliure error");
        } else if (error instanceof ServerError) {
          UtilityPermission.showToast(context, "Server error");
        } else if (error instanceof NetworkError) {
           UtilityPermission.showToast(context, "Network error");
        } else if (error instanceof ParseError) {
           UtilityPermission.showToast(context, "Parse error");
        }
    }

    private void getCategoryDataFromServer() {
        if(check) {
            progresDialog = new ProgressDialog(context);
            progresDialog.setMessage("Please Wait");
            progresDialog.setCancelable(true);
            progresDialog.show();
        }
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(progresDialog.isShowing())
                    progresDialog.dismiss();

                listener.getResponse(response,request_id);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showError(error);
            }
        })
        {
            @Override

            public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> header = new HashMap<String, String>();
            header.put("token", "AFcWxV21C7fd0v3bYYYRCpSSRl31AcsAW04eK1GU5HZCj8LnCCFBd");
           //  header.put("Content-Type", "application/json");
            return header;
        }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(user).getBytes();
            }*/
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
// Adding request to request queue
        ApplicationContextProvider.getInstance().addToRequestQueue(strReq, "tag_string_req");


    }

    private void getResponseFromServer() {
        Log.e("params ",params.toString());
        if(check) {
             progresDialog = new ProgressDialog(context);
            progresDialog.setMessage("Please Wait");
            progresDialog.setCancelable(true);
            progresDialog.show();

        }

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
              if(progresDialog.isShowing())
                    progresDialog.dismiss();
               // holdToLoadLayout.setStopWhenFilled(true);
                listener.getResponse(response,request_id);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                listener.getResponse("error",request_id);
                showError(error);
            }

        }){

           @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  header = new HashMap<String, String>();
                header.put("token","AFcWxV21C7fd0v3bYYYRCpSSRl31AcsAW04eK1GU5HZCj8LnCCFBd");
           //     header.put("Content-Type", "application/json");
                return header;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           // Adding request to request queue
        ApplicationContextProvider.getInstance().addToRequestQueue(strReq, "tag_string_req");

    }

}
