package com.android.webztalk;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.webztalk.AllFragment.EditProfileFragment;
import com.android.webztalk.AllFragment.MainFragment;
import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.AlertManger;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;


public class AfterSplashActivity extends AppCompatActivity {

    private ImageButton login, signup;
    private RelativeLayout relative;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private UserPref user_pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_pref = new UserPref(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                if (newToken == null) {
                    Log.e("FB", "User Logged Out.");
                } else
                    Log.e("FB", "User Loggedin .");

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                // displayMessage(newProfile);
                // displayMessage(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        // getHashKey();
        setContentView(R.layout.activity_after_splash);
      /*  loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));
        loginButton.registerCallback(callbackManager, callback);*/
        initializeUI();
    }

    private void getHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initializeUI() {
        login = (ImageButton) findViewById(R.id.login);
        signup = (ImageButton) findViewById(R.id.signup);
        relative = (RelativeLayout) findViewById(R.id.relative);
        setListener();
    }

    private void setListener() {
        login.setOnClickListener(click_listener);
        signup.setOnClickListener(click_listener);
        relative.setOnClickListener(click_listener);
    }

    private void goToRegister() {
        Intent i = new Intent();
        i.setClass(AfterSplashActivity.this, RegistrationActivity.class);
        startActivity(i);
    }

    private void goToLogin() {
        Intent i = new Intent();
        i.setClass(AfterSplashActivity.this, LoginActivity.class);
        startActivity(i);
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            Log.e("profile name", profile.getName());
        } else
            Log.e("profile is null ", "profile is null");
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == relative) {
                showToast();
            } else if (view == login) {
                goToLogin();
            } else if (view == signup) {
                goToRegister();

            }

        }
    };

    private void showToast() {
        UtilityPermission.showToast(AfterSplashActivity.this, "Please first do login !!");
    }

        /* private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
             @Override
             public void onSuccess(LoginResult loginResult) {
                 final AccessToken accessToken = loginResult.getAccessToken();
                 if (accessToken == null)
                     Log.e("access token is null", "access token is null");
                 else
                     Log.e("access token isn null", "access token is not null" + accessToken.getToken());
                 profileTracker = new ProfileTracker() {
                     @Override
                     protected void onCurrentProfileChanged(
                             Profile oldProfile, Profile currentProfile) {
                         profileTracker.stopTracking();
                         Profile.setCurrentProfile(currentProfile);
                         Profile profile = Profile.getCurrentProfile();
                         displayMessage(profile);
                     }
                 };
                 profileTracker.startTracking();
                 accessTokenTracker = new AccessTokenTracker() {
                     @Override
                     protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                         accessTokenTracker.stopTracking();
                         if (newToken == null) {
                             Log.e("FB", "User Logged Out.");
                         } else
                             Log.e("FB", "User Loggedin .");

                     }
                 };
                 accessTokenTracker.startTracking();
               *//*  Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);*//*
            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.e("Login1Activity", response.toString());
                            // Application code
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("fb_token", accessToken.getToken());
                            CallService.getInstance().passInfromation(api_listener, Constants.FACEBOOK__LOGIN_URL, params, true, "1", AfterSplashActivity.this);
                               *//* user_pref.setUserEmail(object.getString("email"));
                                user_pref.setUserName(object.getString("name"));
                                user_pref.setAutoLogin(true);
                                user_pref.setLoginType("facebook");
                                Intent i = new Intent();
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.setClass(AfterSplashActivity.this, HomeActivity.class);
                                startActivity(i);*//*
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };
*/
      /*  protected void onStop() {
            super.onStop();
            //Facebook login
            accessTokenTracker.stopTracking();
            profileTracker.stopTracking();
        }

        @Override
        public void onResume() {
            super.onResume();
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
        }*/


}
