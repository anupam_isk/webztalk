package com.android.webztalk;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.webztalk.AllFragment.ChangePasswordFragmnt;
import com.android.webztalk.AllFragment.EditProfileFragment;
import com.android.webztalk.AllFragment.HomeFragment;
import com.android.webztalk.AllFragment.MainFragment;
import com.android.webztalk.AllFragment.NavigationDrawerFragment;
import com.android.webztalk.AllFragment.RecommendedFragment;
import com.android.webztalk.FragmentInterface.FragmentListenerInterface;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.UtilityPermission;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

public class HomeActivity extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, FragmentListenerInterface {
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private String TAG;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean value = false;
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private TextView my_web, recommend_web, chat;
    private boolean mUserLearnedDrawer;
    boolean doubleBackToExitPressedOnce = false;
    private boolean mFromSavedInstanceState;
    private Toolbar toolbar;
    private UserPref userpref;
    private ImageView back_arrow, list_menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(Constants.CONSUMER_KEY,Constants.CONSUMER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        generateHashkey();
        setContentView(R.layout.activity_home);
        Log.e("density", String.valueOf(getResources().getDisplayMetrics().density));
        userpref = new UserPref(this);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        // getSupportFragmentManager().beginTransaction().replace(R.id.navigation_drawer,mNavigationDrawerFragment,"t").addToBackStack("navi").commit();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);
        // Set up the drawer.
        setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        initializeToolbar();
        initializeBottomBar();
        setListener();
    }
    public void generateHashkey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.android.webztalk",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

               Log.e("sh",Base64.encodeToString(md.digest(),
                        Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }
    private void setListener() {
        list_menu.setOnClickListener(click_listener);
        back_arrow.setOnClickListener(click_listener);
        my_web.setOnClickListener(click_listener);
        recommend_web.setOnClickListener(click_listener);
        chat.setOnClickListener(click_listener);
    }
    private void initializeBottomBar() {
        my_web = (TextView) findViewById(R.id.my_web);
        recommend_web = (TextView) findViewById(R.id.recommend_web);
        chat = (TextView) findViewById(R.id.chat);
    }
    private void initializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        list_menu = (ImageView) toolbar.findViewById(R.id.list_menu);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        list_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                drawerLayout,                    /* DrawerLayout object */
                toolbar,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.e("drawer close", "drawer close");

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.e("drawer open", "drawer open");
                //mNavigationDrawerFragment.setData();  //okk
                if (!mUserLearnedDrawer) {

                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(HomeActivity.this);
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void onSectionAttached(int number) {
        Fragment fragment = null;
        if (mDrawerLayout != null)
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        switch (number) {
            case 0:
                //  showView(my_web,recommend_web,chat);
                fragment = new HomeFragment();
                replaceFragment(fragment, "withoutbackstack");
                break;
            case 1:
                showToast("Under Development");
                break;
            case 2:
                showToast("Under Development");
                break;
            case 3:
                showToast("Under Development");
                break;

        }
    }
    @Override
    public void onBackPressed() {
        //Checking for fragment count on backstack
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();

        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment = null;
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_two:
                fragment = new EditProfileFragment();
                replaceFragment(fragment, "withbackstack");
                return true;
            case R.id.action_three:
                showView(my_web, recommend_web, chat);
                fragment = new HomeFragment();
                replaceFragment(fragment, "withoutbackstack");
                return true;
            case R.id.action_fourth:
                if (userpref.getLoginType().equals("facebook"))
                    logoutFromFacebook();
                else
                    showPopup();
                return true;
            case R.id.action_fifth:
                fragment = new ChangePasswordFragmnt();
                replaceFragment(fragment, "withbackstack");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void logoutFromFacebook() {
        LoginManager.getInstance().logOut();
        userpref.clearPref();
        Intent i = new Intent(HomeActivity.this, AfterSplashActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(Constants.APP_NAME);
        builder.setMessage("Are you sure you want to logout ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                userpref.clearPref();
                Intent i = new Intent(HomeActivity.this, AfterSplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showToast(String two) {
        Toast.makeText(HomeActivity.this, two, Toast.LENGTH_SHORT).show();
    }
    private void replaceFragment(Fragment fragment, String type) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        if (type.equals("withoutbackstack"))
            fragment_transaction.replace(R.id.container, fragment).commit();
        else
            fragment_transaction.replace(R.id.container, fragment).addToBackStack("").commit();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        onSectionAttached(position);
    }

    @Override
    public void showBackArrow(boolean value) {
        if (value) {
            back_arrow.setVisibility(View.VISIBLE);
            list_menu.setVisibility(View.GONE);
        } else {
            back_arrow.setVisibility(View.GONE);
            list_menu.setVisibility(View.VISIBLE);
        }

    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == list_menu) {
                mDrawerLayout.openDrawer(mFragmentContainerView);
            } else if (v == back_arrow) {
                getSupportFragmentManager().popBackStack();
            } else if (v == my_web) {
                showView(my_web, recommend_web, chat);
                Fragment fragment = new HomeFragment();
                replaceFragment(fragment, "withoutbackstack");
            } else if (v == recommend_web) {
                showView(recommend_web, my_web, chat);
                Fragment fragment = new RecommendedFragment();
                replaceFragment(fragment, "withoutbackstack");
            } else if (v == chat) {
                showView(chat, recommend_web, my_web);
                UtilityPermission.showToast(HomeActivity.this, "Under Development");
            }
        }
    };

    private void showView(TextView my_web, TextView recommend_web, TextView chat) {
        my_web.setBackgroundColor(getResources().getColor(R.color.transparent_color));
        recommend_web.setBackgroundColor(getResources().getColor(R.color.black_color));
        chat.setBackgroundColor(getResources().getColor(R.color.black_color));
    }
    public void showAllViewBlack() {
        my_web.setBackgroundColor(getResources().getColor(R.color.black_color));
        recommend_web.setBackgroundColor(getResources().getColor(R.color.black_color));
        chat.setBackgroundColor(getResources().getColor(R.color.black_color));
    }
}
