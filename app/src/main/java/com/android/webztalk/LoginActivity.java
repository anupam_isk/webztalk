package com.android.webztalk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.webztalk.FragmentInterface.ApiResponseListener;
import com.android.webztalk.ServerCommunication.CallService;
import com.android.webztalk.helper.AlertManger;
import com.android.webztalk.helper.Constants;
import com.android.webztalk.helper.NetworkCheck;
import com.android.webztalk.helper.UserPref;
import com.android.webztalk.helper.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    private ImageButton login;
    private TextView account_text;
    private EditText email, password;
    private UserPref user_pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user_pref = new UserPref(this);
        initializeUI();
    }
    private void initializeUI() {
        login = (ImageButton) findViewById(R.id.login);
        account_text = (TextView) findViewById(R.id.account_text);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToserver();
            }
        });
        account_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);
            }
        });
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    email.setError(null);
                    password.setError(null);
                }
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    email.setError(null);
                    password.setError(null);
                }
            }
        });
    }

    private void sendToserver() {
        if (NetworkCheck.getInstance(this).isNetworkAvailable()) {
            if (validateLogin()) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email.getText().toString());
                params.put("password", password.getText().toString());
                CallService.getInstance().passInfromation(listener, Constants.LOGIN_URL, params, true, "1", LoginActivity.this);
            }
        } else {
            Toast.makeText(LoginActivity.this, Constants.NETWORK_STRING, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateLogin() {
        if (Validation.isValidEmail(email.getText().toString()) && email.getText().toString().length() > 0) {
            if (password.getText().toString().length() > 0) {
                return true;
            } else {
                password.setError("Please Enter Password");
                password.requestFocus();
                return false;
            }
        } else {
            email.setError("Please Enter Valid Email Id");
            email.requestFocus();
            return false;
        }
    }
    ApiResponseListener listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            try {
                JSONObject json_object = new JSONObject(response);
                if (json_object.getBoolean("status")) {
                    JSONObject user_object = json_object.getJSONObject("user_detail");
                    JSONObject follow_object = json_object.getJSONObject("follow_detail");
                    user_pref.setUserId(user_object.getString("id"));
                    user_pref.setUserEmail(user_object.getString("email"));
                    user_pref.setAnotherUserName(user_object.getString("username"));
                    user_pref.setUserName(user_object.getString("name"));
                    user_pref.setUserAddress(user_object.getString("useraddress"));
                    user_pref.setUserBio(user_object.getString("userprofile"));
                    user_pref.setUserPhone(user_object.getString("userphone"));
                    user_pref.setUserImage(user_object.getString("userimage"));
                    user_pref.setAutoLogin(true);
                    user_pref.setFollowerCount(follow_object.getString("follower"));
                    user_pref.setFollowingCount(follow_object.getString("following"));
                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                } else {
                    AlertManger.getAlert_instance().showAlert("Either Email id or Password is invalid.", LoginActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

}

