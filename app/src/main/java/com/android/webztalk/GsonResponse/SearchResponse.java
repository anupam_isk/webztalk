package com.android.webztalk.GsonResponse;

import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.Model.User;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 8/25/2016.
 */
public class SearchResponse {

    public boolean status;
    public AllData data;
    public boolean isStatus() {
        return status;
    }
    public AllData getData() {
        return data;
    }
    public class AllData
    {
        public ArrayList<User> user;
        public ArrayList<MediaObject> media;
        public ArrayList<MediaObject> getMedia() {
            return media;
        }
        public ArrayList<User> getUser() {
            return user;
        }
    }


}
