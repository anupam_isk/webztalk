package com.android.webztalk.GsonResponse;

import com.android.webztalk.Model.User;

import java.util.ArrayList;

/**
 * Created by Administrator on 9/5/2016.
 */
public class FollowingResponse {
    public boolean status;
    public AllData data;

    public boolean isStatus() {
        return status;
    }

    public AllData getData() {
        return data;
    }

    public class AllData
    {
        public ArrayList<User> following;


        public ArrayList<User> getUser() {
            return following;
        }
    }
}
