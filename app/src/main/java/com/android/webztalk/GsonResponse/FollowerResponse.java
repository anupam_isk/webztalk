package com.android.webztalk.GsonResponse;

import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.Model.User;

import java.util.ArrayList;

/**
 * Created by Administrator on 9/5/2016.
 */
public class FollowerResponse {
    public boolean status;
    public AllData data;

    public boolean isStatus() {
        return status;
    }

    public AllData getData() {
        return data;
    }

    public class AllData
    {
        public ArrayList<User> follower;


        public ArrayList<User> getUser() {
            return follower;
        }
    }

}
