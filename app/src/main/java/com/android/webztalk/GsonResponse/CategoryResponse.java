package com.android.webztalk.GsonResponse;

import com.android.webztalk.Model.CategoryCount;
import com.android.webztalk.Model.Filter;
import com.android.webztalk.Model.MediaObject;
import com.android.webztalk.Model.User;

import java.util.ArrayList;

/**
 * Created by Administrator on 8/29/2016.
 */
public class CategoryResponse {
    public boolean status;
    public AllData data;

    public boolean isStatus() {
        return status;
    }

    public AllData getData() {
        return data;
    }

    public class AllData {
        public ArrayList<CategoryCount> categories;
        public ArrayList<MediaObject> category_data;
        public ArrayList<Filter> filters;
        public ArrayList<Filter> getFilters() {
            return filters;
        }

        public ArrayList<CategoryCount> getCategories() {
            return categories;
        }

        public ArrayList<MediaObject> getCategory_data() {
            return category_data;
        }
    }
}
