package com.android.webztalk.Model;

/**
 * Created by Anupam tyagi on 9/14/2016.
 */
public class Filter {
    String id;
    String filtername;
    String categoryname;
    String count;

    public String getColor_code() {
        return color_code;
    }

    String color_code;
    public String getId() {
        return id;
    }
    public String getFiltername() {
        return filtername;
    }

    public void setFiltername(String filtername) {
        this.filtername = filtername;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }
    public String getCount() {
        return count;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
