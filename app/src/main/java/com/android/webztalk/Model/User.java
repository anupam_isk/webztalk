package com.android.webztalk.Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Administrator on 8/25/2016.
 */
public class User implements Parcelable {

    String id;
    String firstname;
    String useremail;
    String userimage;
    String userphone;
    String useraddress;
    String follow_status;
    String following;
    String follower;
    ArrayList<CategoryCount> categories;

    public ArrayList<CategoryCount> getCategories() {
        return categories;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    String username;
    public String getFollower() {
        return follower;
    }
    public void setFollower(String follower) {
        this.follower = follower;
    }
    public String getFollowing() {
        return following;
    }
    public void setFollowing(String following) {
        this.following = following;
    }
    protected User(Parcel in) {
        id = in.readString();
        firstname = in.readString();
        useremail = in.readString();
        userimage = in.readString();
        follower = in.readString();
        following = in.readString();
        userphone = in.readString();
        useraddress = in.readString();
        follow_status = in.readString();
        username = in.readString();
        categories=in.createTypedArrayList(CategoryCount.CREATOR);
    }
    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setFollow_status(String follow_status) {
        this.follow_status = follow_status;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getUseremail() {
        return useremail;
    }

    public String getUserimage() {
        return userimage;
    }

    public String getUserphone() {
        return userphone;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public String getFollow_status() {
        return follow_status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(firstname);
        parcel.writeString(useremail);
        parcel.writeString(userimage);
        parcel.writeString(userphone);
        parcel.writeString(useraddress);
        parcel.writeString(follow_status);
        parcel.writeString(follower);
        parcel.writeString(following);
        parcel.writeString(username);
        parcel.writeTypedList(categories);
    }
}
