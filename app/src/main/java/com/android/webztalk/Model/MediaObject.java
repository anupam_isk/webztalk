package com.android.webztalk.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anupam on 8/25/2016.
 */
public class MediaObject implements Parcelable {

    String id;
    String categoryid;
    String mediatitle;
    String recently_id;
    String mediadescription;
    String image;
    String imageflag;
    String youtube_url;
    String release_date;
    String color;
    String rating_color;
    String user_rating;
    String rating;

    public String getIn_common() {
        return in_common;
    }

    String in_common;
    String firstname,like,dislike,mediatype;

    public String getFirstname() {
        return firstname;
    }

    public String getDislike() {

        return dislike;
    }

    public void setDislike(String dislike) {
        this.dislike = dislike;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    int delete_status=0;

    public int getDelete_status() {
        return delete_status;
    }

    public String getMediatype() {
        return mediatype;
    }



    public void setDelete_status(int delete_status) {
        this.delete_status = delete_status;
    }

    public String getRating_color() {
        return rating_color;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public String getRating() {
        return rating;
    }

    String word;
    String categoryname;

    protected MediaObject(Parcel in) {
        id = in.readString();
        categoryid = in.readString();
        mediatitle = in.readString();
        mediadescription = in.readString();
        image = in.readString();
        imageflag = in.readString();
        youtube_url = in.readString();
        release_date = in.readString();
        categoryname = in.readString();
        color = in.readString();
        word = in.readString();
        mediatype=in.readString();
    }

    public static final Creator<MediaObject> CREATOR = new Creator<MediaObject>() {
        @Override
        public MediaObject createFromParcel(Parcel in) {
            return new MediaObject(in);
        }

        @Override
        public MediaObject[] newArray(int size) {
            return new MediaObject[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getCategoryid() {
        return categoryid;
    }


    public String getMediatitle() {
        return mediatitle;
    }


    public String getMediadescription() {
        return mediadescription;
    }

    public String getImage() {
        return image;
    }

    public String getImageflag() {
        return imageflag;
    }


    public String getYoutube_url() {
        return youtube_url;
    }


    public String getRelease_date() {
        return release_date;
    }


    public String getCategoryname() {
        return categoryname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getColor() {
        return color;
    }

    public String getRecently_id() {
        return recently_id;
    }

    public String getWord() {
        return word;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(categoryid);
        parcel.writeString(mediatitle);
        parcel.writeString(mediadescription);
        parcel.writeString(image);
        parcel.writeString(imageflag);
        parcel.writeString(youtube_url);
        parcel.writeString(release_date);
        parcel.writeString(word);
        parcel.writeString(color);
        parcel.writeString(categoryname);
        parcel.writeString(mediatype);
    }
}
