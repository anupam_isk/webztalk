package com.android.webztalk.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 8/29/2016.
 */
public class CategoryCount implements Parcelable {
    String id,categoryname,count;

    protected CategoryCount(Parcel in) {
        id = in.readString();
        categoryname = in.readString();
        count = in.readString();
    }

    public static final Creator<CategoryCount> CREATOR = new Creator<CategoryCount>() {
        @Override
        public CategoryCount createFromParcel(Parcel in) {
            return new CategoryCount(in);
        }

        @Override
        public CategoryCount[] newArray(int size) {
            return new CategoryCount[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public String getCount() {
        return count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(categoryname);
        dest.writeString(count);
    }

}
